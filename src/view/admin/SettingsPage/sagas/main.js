import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as settingsAPI from "api/admin/settings";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "react-notifications";

export function* handleGetSettings(action) {
  try {
    let res = yield call(settingsAPI.getSettings, action.payload);
    if (res.data.success) {
      yield put(actions.getSettingsSuccess(res.data.data));
    } else {
      yield put(actions.getSettingsFail(res.data.message));
      NotificationManager.error("Kiểm tra lại đường truyền !", "Lỗi", 2000);
    }
  } catch (error) {
    yield put(actions.getSettingsFail(error));
  }
}

export function* handleUpdateSettings(action) {
  try {
    let res = yield call(settingsAPI.updateSettings, action.payload);
    if (res.data.success) {
      yield put(actions.updateSettingsSuccess(res.data.data));
      NotificationManager.success("Cập nhật thành công !", "Thông báo", 2000);
    } else {
      yield put(actions.updateSettingsFail(res.data.message));
      NotificationManager.error("Cập nhật thất bại !", "Lỗi", 2000);
    }
  } catch (error) {
    yield put(actions.updateSettingsFail(error));
  }
}

/*---------------------------------------------------------------------*/
export function* getSettings() {
  yield takeAction(actions.getSettings, handleGetSettings);
}

export function* updateSettings() {
  yield takeAction(actions.updateSettings, handleUpdateSettings);
}
/*---------------------------------------------------------------------*/

export default [getSettings, updateSettings];
