/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

export const getSettings = createAction(CONST.GET_SETTINGS);
export const getSettingsSuccess = createAction(CONST.GET_SETTINGS_SUCCESS);
export const getSettingsFail = createAction(CONST.GET_SETTINGS_FAIL);

export const updateSettings = createAction(CONST.UPDATE_SETTINGS);
export const updateSettingsSuccess = createAction(CONST.UPDATE_SETTINGS_SUCCESS);
export const updateSettingsFail = createAction(CONST.UPDATE_SETTINGS_FAIL);

/*-----------------------------------------------------------------------*/ 

export const handleChangeTab = createAction(CONST.HANDLE_CHANGE_TAB);
export const handleValidate = createAction(CONST.HANDLE_VALIDATE);
export const handleInputChange = createAction(CONST.HANDLE_INPUT_CHANGE);
export const handleSelectChange = createAction(CONST.HANDLE_SELECT_CHANGE);

export const handleClear = createAction(CONST.HANDLE_CLEAR);
