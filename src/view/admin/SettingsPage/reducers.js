/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import _ from "lodash";

export const name = "AdminSettingsPage";

const initialState = freeze({
  priceCalculateOptions: [
    { value: false, label: "Ngày thường" },
    { value: true, label: "Ngày lễ" }
  ],
  activeTab: "1",
  data: {},
  settings: {},
  isProgressing: false,
  priceCalculateSelected: {},
  priceWeekendSelected: {},
  /*--------*/
  pricePerHourCommonDayCommonRoomError: false,
  pricePer12HourCommonDayCommonRoomError: false,
  pricePerHourHolidayCommonRoomError: false,
  pricePer12HourHolidayCommonRoomError: false,
  /*--------*/
  pricePerHourCommonDayColdRoomError: false,
  pricePer12HourCommonDayColdRoomError: false,
  pricePerHourHolidayColdRoomError: false,
  pricePer12HourHolidayColdRoomError: false,
  /*--------*/
  extraTimeError: false
});

export default handleActions(
  {
    /*------
    -------- Handle Clear
    */
    [actions.handleClear]: (state, action) => {
      return freeze({
        ...state,
        activeTab: "1",
        data: {},
        /*--------*/
        pricePerHourCommonDayCommonRoomError: false,
        pricePer12HourCommonDayCommonRoomError: false,
        pricePerHourHolidayCommonRoomError: false,
        pricePer12HourHolidayCommonRoomError: false,
        /*--------*/
        pricePerHourCommonDayColdRoomError: false,
        pricePer12HourCommonDayColdRoomError: false,
        pricePerHourHolidayColdRoomError: false,
        pricePer12HourHolidayColdRoomError: false
      });
    },
    /*------
    -------- Handle Tab Change
    */
    [actions.handleChangeTab]: (state, action) => {
      if (state.isProgressing) {
        return freeze({
          ...state
        });
      } else {
        return freeze({
          ...state,
          activeTab: action.payload
        });
      }
    },
    /*------
    -------- Handle Input Change
    */
    [actions.handleInputChange]: (state, action) => {
      let event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      var numberPattern = new RegExp(/^[0-9]*$/);

      if (name === "pricePerHourCommonDayCommonRoom") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            pricePerHourCommonDayCommonRoomError: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          if (!numberPattern.test(value)) {
            return freeze({
              ...state,
              pricePerHourCommonDayCommonRoomError: true,
              data: {
                ...state.data,
                [name]: value
              }
            });
          } else {
            return freeze({
              ...state,
              pricePerHourCommonDayCommonRoomError: false,
              data: {
                ...state.data,
                [name]: value
              }
            });
          }
        }
      }

      if (name === "pricePer12HourCommonDayCommonRoom") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            pricePer12HourCommonDayCommonRoomError: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          if (numberPattern.test(value)) {
            return freeze({
              ...state,
              pricePer12HourCommonDayCommonRoomError: false,
              data: {
                ...state.data,
                [name]: value
              }
            });
          } else {
            return freeze({
              ...state,
              pricePer12HourCommonDayCommonRoomError: true,
              data: {
                ...state.data,
                [name]: value
              }
            });
          }
        }
      }

      if (name === "pricePerHourHolidayCommonRoom") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            pricePerHourHolidayCommonRoomError: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          if (numberPattern.test(value)) {
            return freeze({
              ...state,
              pricePerHourHolidayCommonRoomError: false,
              data: {
                ...state.data,
                [name]: value
              }
            });
          } else {
            return freeze({
              ...state,
              pricePerHourHolidayCommonRoomError: true,
              data: {
                ...state.data,
                [name]: value
              }
            });
          }
        }
      }

      if (name === "pricePer12HourHolidayCommonRoom") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            pricePer12HourHolidayCommonRoomError: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          if (numberPattern.test(value)) {
            return freeze({
              ...state,
              pricePer12HourHolidayCommonRoomError: false,
              data: {
                ...state.data,
                [name]: value
              }
            });
          } else {
            return freeze({
              ...state,
              pricePer12HourHolidayCommonRoomError: true,
              data: {
                ...state.data,
                [name]: value
              }
            });
          }
        }
      }

      if (name === "pricePerHourCommonDayColdRoom") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            pricePerHourCommonDayColdRoomError: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          if (numberPattern.test(value)) {
            return freeze({
              ...state,
              pricePerHourCommonDayColdRoomError: false,
              data: {
                ...state.data,
                [name]: value
              }
            });
          } else {
            return freeze({
              ...state,
              pricePerHourCommonDayColdRoomError: true,
              data: {
                ...state.data,
                [name]: value
              }
            });
          }
        }
      }

      if (name === "pricePer12HourCommonDayColdRoom") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            pricePer12HourCommonDayColdRoomError: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          if (numberPattern.test(value)) {
            return freeze({
              ...state,
              pricePer12HourCommonDayColdRoomError: false,
              data: {
                ...state.data,
                [name]: value
              }
            });
          } else {
            return freeze({
              ...state,
              pricePer12HourCommonDayColdRoomError: true,
              data: {
                ...state.data,
                [name]: value
              }
            });
          }
        }
      }

      if (name === "pricePerHourHolidayColdRoom") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            pricePerHourHolidayColdRoomError: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          if (numberPattern.test(value)) {
            return freeze({
              ...state,
              pricePerHourHolidayColdRoomError: false,
              data: {
                ...state.data,
                [name]: value
              }
            });
          } else {
            return freeze({
              ...state,
              pricePerHourHolidayColdRoomError: true,
              data: {
                ...state.data,
                [name]: value
              }
            });
          }
        }
      }

      if (name === "pricePer12HourHolidayColdRoom") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            pricePer12HourHolidayColdRoomError: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          if (numberPattern.test(value)) {
            return freeze({
              ...state,
              pricePer12HourHolidayColdRoomError: false,
              data: {
                ...state.data,
                [name]: value
              }
            });
          } else {
            return freeze({
              ...state,
              pricePer12HourHolidayColdRoomError: true,
              data: {
                ...state.data,
                [name]: value
              }
            });
          }
        }
      }

      if (name === "extraTime") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            extraTimeError: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          if (numberPattern.test(value)) {
            return freeze({
              ...state,
              extraTimeError: false,
              data: {
                ...state.data,
                [name]: value
              }
            });
          } else {
            return freeze({
              ...state,
              extraTimeError: true,
              data: {
                ...state.data,
                [name]: value
              }
            });
          }
        }
      }

      return freeze({
        ...state,
        data: {
          ...state.data,
          [name]: value
        }
      });
    },
    /*------
    -------- Handle Validate
    */
    [actions.handleValidate]: (state, action) => {
      if (action.payload.roomType === "normal") {
        if (action.payload.dayType === "normal") {
          if (action.payload.price === "perHour") {
            return freeze({
              ...state,
              pricePerHourCommonDayCommonRoomError: true
            });
          }
          if (action.payload.price === "per12Hour") {
            return freeze({
              ...state,
              pricePer12HourCommonDayCommonRoomError: true
            });
          }
        } else {
          if (action.payload.price === "perHour") {
            return freeze({
              ...state,
              pricePerHourHolidayCommonRoomError: true
            });
          }
          if (action.payload.price === "per12Hour") {
            return freeze({
              ...state,
              pricePer12HourHolidayCommonRoomError: true
            });
          }
        }
      }

      /*----------------------------------------*/

      if (action.payload.roomType === "cold") {
        if (action.payload.dayType === "normal") {
          if (action.payload.price === "perHour") {
            return freeze({
              ...state,
              pricePerHourCommonDayColdRoomError: true
            });
          }
          if (action.payload.price === "per12Hour") {
            return freeze({
              ...state,
              pricePer12HourCommonDayColdRoomError: true
            });
          }
        } else {
          if (action.payload.price === "perHour") {
            return freeze({
              ...state,
              pricePerHourHolidayColdRoomError: true
            });
          }
          if (action.payload.price === "per12Hour") {
            return freeze({
              ...state,
              pricePer12HourHolidayColdRoomError: true
            });
          }
        }
      }

      if (action.payload === "all") {
        return freeze({
          ...state,
          pricePerHourCommonDayCommonRoomError: true,
          pricePer12HourCommonDayCommonRoomError: true,
          pricePerHourHolidayCommonRoomError: true,
          pricePer12HourHolidayCommonRoomError: true,
          /*--------*/
          pricePerHourCommonDayColdRoomError: true,
          pricePer12HourCommonDayColdRoomError: true,
          pricePerHourHolidayColdRoomError: true,
          pricePer12HourHolidayColdRoomError: true,
          /*--------*/
          extraTimeError: true
        });
      }

      if (action.payload === "extraTime") {
        return freeze({
          ...state,
          extraTimeError: true
        });
      }

      return freeze({
        ...state
      });
    },
    /*------
    -------- Handle Select Change
    */
    [actions.handleSelectChange]: (state, action) => {
      let settings = state.settings;
      if (action.payload.selectType === "holiday") {
        if (action.payload.option.value) {
          return freeze({
            ...state,
            data: {
              ...state.data,
              setTodayIsHoliday: action.payload.option.value,
              setWeekendIsHoliday: action.payload.option.value,
              pricePerHourCommonRoom: settings.pricePerHourHolidayCommonRoom,
              pricePer12HourCommonRoom:
                settings.pricePer12HourHolidayCommonRoom,
              pricePerHourColdRoom: settings.pricePerHourHolidayColdRoom,
              pricePer12HourColdRoom: settings.pricePer12HourHolidayColdRoom
            },
            priceCalculateSelected: action.payload.option,
            priceWeekendSelected: action.payload.option
          });
        } else {
          return freeze({
            ...state,
            data: {
              ...state.data,
              setTodayIsHoliday: action.payload.option.value,
              pricePerHourCommonRoom: settings.pricePerHourCommonDayCommonRoom,
              pricePer12HourCommonRoom:
                settings.pricePer12HourCommonDayCommonRoom,
              pricePerHourColdRoom: settings.pricePerHourCommonDayColdRoom,
              pricePer12HourColdRoom: settings.pricePer12HourCommonDayColdRoom
            },
            priceCalculateSelected: action.payload.option
          });
        }
      }
      if (action.payload.selectType === "weekendPrice") {
        return freeze({
          ...state,
          data: {
            ...state.data,
            setWeekendIsHoliday: action.payload.option.value
          },
          priceWeekendSelected: action.payload.option
        });
      }

      return freeze({
        ...state
      });
    },
    /*------
    -------- Get Settings
    */
    [actions.getSettingsSuccess]: (state, action) => {
      const index = state.priceCalculateOptions.findIndex(
        item => item.value === action.payload.setTodayIsHoliday
      );
      const indexWeekend = state.priceCalculateOptions.findIndex(
        item => item.value === action.payload.setWeekendIsHoliday
      );

      let extraTime = action.payload.extraTime.toString();
      let pricePer12HourCommonDayColdRoom = action.payload.pricePer12HourCommonDayColdRoom.toString();
      let pricePer12HourCommonDayCommonRoom = action.payload.pricePer12HourCommonDayCommonRoom.toString();
      let pricePer12HourHolidayColdRoom = action.payload.pricePer12HourHolidayColdRoom.toString();
      let pricePer12HourHolidayCommonRoom = action.payload.pricePer12HourHolidayCommonRoom.toString();
      let pricePerHourCommonDayColdRoom = action.payload.pricePerHourCommonDayColdRoom.toString();
      let pricePerHourCommonDayCommonRoom = action.payload.pricePerHourCommonDayCommonRoom.toString();
      let pricePerHourHolidayColdRoom = action.payload.pricePerHourHolidayColdRoom.toString();
      let pricePerHourHolidayCommonRoom = action.payload.pricePerHourHolidayCommonRoom.toString();

      let temptData = {
        ...action.payload,
        extraTime,
        pricePer12HourCommonDayColdRoom,
        pricePer12HourCommonDayCommonRoom,

        pricePer12HourHolidayColdRoom,
        pricePer12HourHolidayCommonRoom,

        pricePerHourCommonDayColdRoom,
        pricePerHourCommonDayCommonRoom,

        pricePerHourHolidayColdRoom,
        pricePerHourHolidayCommonRoom
      };
      if (action.payload.setTodayIsHoliday) {
        temptData = {
          ...temptData,
          pricePerHourCommonRoom: action.payload.pricePerHourHolidayCommonRoom,
          pricePer12HourCommonRoom:
            action.payload.pricePer12HourHolidayCommonRoom,
          pricePerHourColdRoom: action.payload.pricePerHourHolidayColdRoom,
          pricePer12HourColdRoom: action.payload.pricePer12HourHolidayColdRoom
        };
      } else {
        temptData = {
          ...temptData,
          pricePerHourCommonRoom:
            action.payload.pricePerHourCommonDayCommonRoom,
          pricePer12HourCommonRoom:
            action.payload.pricePer12HourCommonDayCommonRoom,
          pricePerHourColdRoom: action.payload.pricePerHourCommonDayColdRoom,
          pricePer12HourColdRoom: action.payload.pricePer12HourCommonDayColdRoom
        };
      }

      return freeze({
        ...state,
        data: temptData,
        settings: action.payload,
        priceCalculateSelected: state.priceCalculateOptions[index],
        priceWeekendSelected: state.priceCalculateOptions[indexWeekend]
      });
    },
    /*------
    -------- Get Settings
    */
    [actions.updateSettings]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: true
      });
    },
    [actions.updateSettingsSuccess]: (state, action) => {
      const index = state.priceCalculateOptions.findIndex(
        item => item.value === action.payload.setTodayIsHoliday
      );
      const indexWeekend = state.priceCalculateOptions.findIndex(
        item => item.value === action.payload.setWeekendIsHoliday
      );

      let extraTime = action.payload.extraTime.toString();
      let pricePer12HourCommonDayColdRoom = action.payload.pricePer12HourCommonDayColdRoom.toString();
      let pricePer12HourCommonDayCommonRoom = action.payload.pricePer12HourCommonDayCommonRoom.toString();
      let pricePer12HourHolidayColdRoom = action.payload.pricePer12HourHolidayColdRoom.toString();
      let pricePer12HourHolidayCommonRoom = action.payload.pricePer12HourHolidayCommonRoom.toString();
      let pricePerHourCommonDayColdRoom = action.payload.pricePerHourCommonDayColdRoom.toString();
      let pricePerHourCommonDayCommonRoom = action.payload.pricePerHourCommonDayCommonRoom.toString();
      let pricePerHourHolidayColdRoom = action.payload.pricePerHourHolidayColdRoom.toString();
      let pricePerHourHolidayCommonRoom = action.payload.pricePerHourHolidayCommonRoom.toString();

      let temptData = {
        ...action.payload,
        extraTime,
        pricePer12HourCommonDayColdRoom,
        pricePer12HourCommonDayCommonRoom,
        
        pricePer12HourHolidayColdRoom,
        pricePer12HourHolidayCommonRoom,

        pricePerHourCommonDayColdRoom,
        pricePerHourCommonDayCommonRoom,

        pricePerHourHolidayColdRoom,
        pricePerHourHolidayCommonRoom
      };
      if (action.payload.setTodayIsHoliday) {
        temptData = {
          ...temptData,
          pricePerHourCommonRoom: action.payload.pricePerHourHolidayCommonRoom,
          pricePer12HourCommonRoom:
            action.payload.pricePer12HourHolidayCommonRoom,
          pricePerHourColdRoom: action.payload.pricePerHourHolidayColdRoom,
          pricePer12HourColdRoom: action.payload.pricePer12HourHolidayColdRoom
        };
      } else {
        temptData = {
          ...temptData,
          pricePerHourCommonRoom:
            action.payload.pricePerHourCommonDayCommonRoom,
          pricePer12HourCommonRoom:
            action.payload.pricePer12HourCommonDayCommonRoom,
          pricePerHourColdRoom: action.payload.pricePerHourCommonDayColdRoom,
          pricePer12HourColdRoom: action.payload.pricePer12HourCommonDayColdRoom
        };
      }

      return freeze({
        ...state,
        isProgressing: false,
        data: temptData,
        settings: action.payload,
        priceCalculateSelected: state.priceCalculateOptions[index],
        priceWeekendSelected: state.priceCalculateOptions[indexWeekend]
      });
    },
    [actions.updateSettingsFail]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false
      });
    }
  },
  initialState
);
