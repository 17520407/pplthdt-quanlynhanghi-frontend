import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import classnames from "classnames";

class SettingsTopBar extends Component {
  render() {
    const { activeTab } = this.props;
    return (
      <React.Fragment>
        <Row>
          <Col lg="6" md="12" sm="12">
            <button
              type="button"
              className={classnames(
                "btn btn-primary btn-rounded w-md waves-effect waves-light m-b-20",
                { "btn-trans": activeTab !== "1" }
              )}
              onClick={() => this.props.actions.handleChangeTab("1")}
            >
              <i className="mdi mdi-information-variant"></i>
              &nbsp;Thông tin
            </button>
            &nbsp;
            <button
              type="button"
              className={classnames(
                "btn btn-primary btn-rounded w-md waves-effect waves-light m-b-20",
                { "btn-trans": activeTab !== "2" }
              )}
              onClick={() => this.props.actions.handleChangeTab("2")}
            >
              <i className="mdi mdi-settings-outline"></i>
              &nbsp;Chỉnh sửa
            </button>
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

export default SettingsTopBar;
