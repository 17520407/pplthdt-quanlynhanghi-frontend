import React, { Component } from "react";
import { Row, Col, FormGroup, Label, FormText } from "reactstrap";
import Select from "react-select";
import _ from "lodash";

class SettingsInformation extends Component {
  componentDidUpdate(prevState) {
    const { priceCalculateSelected, priceWeekendSelected } = this.props;
    if (!_.isEmpty(priceCalculateSelected)) {
      if (prevState.priceCalculateSelected !== priceCalculateSelected) {
        this.props.actions.updateSettings({
          setTodayIsHoliday: priceCalculateSelected.value,
          setWeekendIsHoliday: priceWeekendSelected.value
        });
      }
    }

    if (!_.isEmpty(priceWeekendSelected)) {
      if (prevState.priceWeekendSelected !== priceWeekendSelected) {
        this.props.actions.updateSettings({
          setWeekendIsHoliday: priceWeekendSelected.value
        });
      }
    }
  }

  render() {
    const {
      setTodayIsHoliday,
      extraTime,
      pricePerHourCommonRoom,
      pricePer12HourCommonRoom,
      pricePerHourColdRoom,
      pricePer12HourColdRoom
    } = this.props.data;
    const {
      priceCalculateOptions,
      priceCalculateSelected,
      priceWeekendSelected
    } = this.props;
    return (
      <React.Fragment>
        <div className="card-box">
          <Row>
            <Col lg="6">
              <FormGroup>
                <Label>
                  Trạng thái giá tiền bắt đầu từ ngày hiện tại trở đi:
                </Label>
                <Select
                  className="z-Index"
                  options={priceCalculateOptions}
                  onChange={option =>
                    this.props.actions.handleSelectChange({
                      option,
                      selectType: "holiday"
                    })
                  }
                  value={priceCalculateSelected}
                />
              </FormGroup>
            </Col>
            <Col lg="6">
              <FormGroup>
                <Label>
                  Trạng thái giá tiền các ngày cuối tuần (thứ 7, chủ nhật):
                </Label>
                <Select
                  className="z-Index"
                  options={priceCalculateOptions}
                  onChange={option =>
                    this.props.actions.handleSelectChange({
                      option,
                      selectType: "weekendPrice"
                    })
                  }
                  isDisabled={setTodayIsHoliday}
                  value={priceWeekendSelected}
                />
              </FormGroup>
            </Col>
          </Row>
          <hr />
          <Row>
            <Col lg="6" md="12" sm="12">
              <h3>Phòng thường</h3>
              <p>Giá tiền theo giờ : {pricePerHourCommonRoom} VNĐ</p>
              <p>Giá tiền qua đêm : {pricePer12HourCommonRoom} VNĐ</p>
              <FormGroup>
                <p>Thời gian thêm : {extraTime} phút</p>
                <FormText>
                  Thời gian trì hoãn để cộng thêm tiền nếu khách ra trễ
                </FormText>
              </FormGroup>
            </Col>
            <Col lg="6" md="12" sm="12">
              <h3>Phòng máy lạnh</h3>
              <p>Giá tiền theo giờ: {pricePerHourColdRoom} VNĐ</p>
              <p>Giá tiền qua đêm: {pricePer12HourColdRoom} VNĐ</p>
            </Col>
          </Row>
        </div>
      </React.Fragment>
    );
  }
}

export default SettingsInformation;
