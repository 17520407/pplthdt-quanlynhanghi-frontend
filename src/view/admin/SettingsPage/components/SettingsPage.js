import React, { Component } from "react";
// import { Row, Col, Input, FormGroup, FormFeedback, Label } from "reactstrap";
import SettingsTopBar from "./SettingsTopBar";
import SettingsInformation from "./SettingsInformation";
import SettingsForm from "./SettingsForm";

class SettingsPage extends Component {
  render() {
    const { activeTab } = this.props;
    return (
      <React.Fragment>
        <SettingsTopBar {...this.props} />
        {activeTab === "1" && <SettingsInformation {...this.props}/>}
        {activeTab === "2" && <SettingsForm {...this.props}/>}
      </React.Fragment>
    );
  }
}

export default SettingsPage;
