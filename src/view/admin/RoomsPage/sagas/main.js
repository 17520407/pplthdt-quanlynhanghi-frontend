import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import { takeAction } from "services/forkActionSagas";
import * as roomAPI from "api/admin/rooms";
import { NotificationManager } from "react-notifications";
// import _ from "lodash";

export function* handleGetAllRooms(action) {
  try {
    let res = yield call(roomAPI.getAllRooms);
    yield put(actions.getAllRoomsSuccess(res.data.data));
  } catch (err) {
    yield put(actions.getAllRoomsFail(err));
  }
}

export function* handleAddNewRoom(action) {
  try {
    let res = yield call(roomAPI.addNewRoom, action.payload);
    yield put(actions.addNewRoomSuccess(res.data.data));
    NotificationManager.success("Thêm phòng thành công !", "Thông báo", 2000);
  } catch (err) {
    yield put(actions.addNewRoomFail(err));
  }
}

export function* handleUpdateRoom(action) {
  try {
    let res = yield call(roomAPI.updateRoom, action.payload);
    yield put(actions.updateRoomSuccess(res.data.data));
    NotificationManager.success(
      "Chỉnh sửa phòng thành công !",
      "Thông báo",
      2000
    );
  } catch (err) {
    yield put(actions.updateRoomFail(err));
  }
}

export function* handleDelRoom(action) {
  try {
    let res = yield call(roomAPI.delRoom, action.payload);
    if (res.status === 406) {
      NotificationManager.error("Phòng đang có khách hàng, không thể xoá !", "Thông báo", 2000);
      yield put(actions.delRoomFail(res.data.message));
    } else {
      yield put(actions.delRoomSuccess(res.data.data));
      NotificationManager.success("Xoá phòng thành công !", "Thông báo", 2000);
    }
  } catch (err) {
    yield put(actions.delRoomFail(err));
  }
}

/*---------------------------------------------------------------------*/
// Get All Rooms
export function* getAllRooms() {
  yield takeAction(actions.getAllRooms, handleGetAllRooms);
}
// Add new
export function* addNewRoom() {
  yield takeAction(actions.addNewRoom, handleAddNewRoom);
}
// Update room
export function* updateRoom() {
  yield takeAction(actions.updateRoom, handleUpdateRoom);
}
// Del room
export function* delRoom() {
  yield takeAction(actions.delRoom, handleDelRoom);
}

/*---------------------------------------------------------------------*/

export default [getAllRooms, addNewRoom, updateRoom, delRoom];
