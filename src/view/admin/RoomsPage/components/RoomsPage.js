import React, { Component } from "react";
import RoomTopBar from "./RoomTopBar";
import RoomForm from "./RoomForm";
import RoomList from "./RoomsList";
import RoomConfirmModal from "./RoomConfirmModal";

class RoomsPage extends Component {
  render() {
    return (
      <React.Fragment>
        <RoomTopBar {...this.props} />
        <RoomForm {...this.props} />
        <RoomList {...this.props} />
        <RoomConfirmModal {...this.props} />
      </React.Fragment>
    );
  }
}

export default RoomsPage;
