import React, { Component } from "react";
import { Row, Col, FormGroup, FormFeedback, Input } from "reactstrap";
import Select from "react-select";
import _ from "lodash";

class RoomForm extends Component {
  _handleCreateRoom = () => {
    const { name, type, status } = this.props.data;
    const { isUpdate } = this.props;
    if (_.isEmpty(name) && _.isEmpty(type) && _.isEmpty(status)) {
      this.props.actions.handleValidate({ checkType: "room" });
    } else {
      if (_.isEmpty(name)) {
        this.props.actions.handleValidate({ name: "name", checkType: "room" });
      } else if (_.isEmpty(type)) {
        this.props.actions.handleValidate({ name: "type", checkType: "room" });
      } else {
        isUpdate
          ? this.props.actions.updateRoom(this.props.data)
          : this.props.actions.addNewRoom(this.props.data);
        this.props.actions.handleInputClear();
      }
    }
  };

  _handleCloseForm = () => {
    this.props.actions.toggleForm("closeForm");
  }

  render() {
    const { name } = this.props.data;
    const {
      toggleForm,
      isErrorRoomName,
      isErrorRoomType,
      roomType,
      roomStatus,
      typeOptions,
      statusOptions,
      isProgressing,
      isUpdate
    } = this.props;
    return (
      <React.Fragment>
        {toggleForm ? (
          <div className="card-box">
            <Row>
              <Col lg="6">
                <FormGroup>
                  <label>Tên Phòng</label>
                  <Input
                    type="text"
                    name="name"
                    placeholder="Nhập tên phòng"
                    value={name}
                    onChange={e => this.props.actions.handleInputChange(e)}
                    invalid={isErrorRoomName}
                    disabled={isProgressing}
                  />
                  <FormFeedback>Bạn chưa nhập tên phòng !</FormFeedback>
                </FormGroup>
                <FormGroup>
                  <label>Loại Phòng</label>
                  <Select
                    options={typeOptions}
                    onChange={option =>
                      this.props.actions.handleSelectChange({
                        option,
                        selectType: "type"
                      })
                    }
                    value={roomType}
                    isDisabled={isProgressing}
                  />
                  <Input type="hidden" invalid={isErrorRoomType} />
                  <FormFeedback>Bạn chưa chọn loại phòng !</FormFeedback>
                </FormGroup>
              </Col>
              <Col lg="6">
                <FormGroup>
                  <label>Trạng thái</label>
                  <Select
                    className="z-Index"
                    options={statusOptions}
                    onChange={option =>
                      this.props.actions.handleSelectChange({
                        option,
                        selectType: "status"
                      })
                    }
                    value={roomStatus}
                    isDisabled={isProgressing}
                  />
                </FormGroup>
                <FormGroup>
                  <div className="float-right m-t-20">
                    <button
                      type="button"
                      className="btn btn-primary btn-rounded w-md waves-effect waves-light"
                      onClick={this._handleCreateRoom}
                      disabled={isProgressing}
                    >
                      {isProgressing ? (
                        <div>
                          <div className="spinner-border spinner-custom text-custom">
                            <span className="sr-only">Loading...</span>
                          </div>
                          &nbsp; Đang xử lí...
                        </div>
                      ) : (
                        <span>
                          <i className="mdi mdi-arrow-collapse-down" />
                          &nbsp;{isUpdate ? "Lưu thay đổi" : "Thêm mới"}
                        </span>
                      )}
                    </button>
                    &nbsp;
                    <button
                      type="button"
                      className="btn btn-secondary btn-rounded w-md waves-effect waves-light"
                      disabled={isProgressing}
                      onClick={this._handleCloseForm}
                    >
                      Huỷ
                    </button>
                  </div>
                </FormGroup>
              </Col>
            </Row>
          </div>
        ) : null}
      </React.Fragment>
    );
  }
}

export default RoomForm;
