/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

export const toggleForm = createAction(CONST.TOGGLE_FORM);
export const toggleModal = createAction(CONST.TOGGLE_MODAL);

/*---------------------------------------------------------------------*/

export const getAllRooms = createAction(CONST.GET_ALL_ROOMS);
export const getAllRoomsSuccess = createAction(CONST.GET_ALL_ROOMS_SUCCESS);
export const getAllRoomsFail = createAction(CONST.GET_ALL_ROOMS_FAIL);

export const addNewRoom = createAction(CONST.ADD_NEW_ROOM);
export const addNewRoomSuccess = createAction(CONST.ADD_NEW_ROOM_SUCCESS);
export const addNewRoomFail = createAction(CONST.ADD_NEW_ROOM_FAIL);

export const updateRoom = createAction(CONST.UPDATE_ROOM);
export const updateRoomSuccess = createAction(CONST.UPDATE_ROOM_SUCCESS);
export const updateRoomFail = createAction(CONST.UPDATE_ROOM_FAIL);

export const delRoom = createAction(CONST.DEL_ROOM);
export const delRoomSuccess = createAction(CONST.DEL_ROOM_SUCCESS);
export const delRoomFail = createAction(CONST.DEL_ROOM_FAIL);

/*---------------------------------------------------------------------*/

export const handleRoomDetail = createAction(CONST.HANDLE_ROOM_DETAIL);
export const handleInputClear = createAction(CONST.HANDLE_INPUT_CLEAR);
export const handleInputChange = createAction(CONST.HANDLE_INPUT_CHANGE);
export const handleSelectChange = createAction(CONST.HANDLE_SELECT_CHANGE);
export const handleValidate = createAction(CONST.HANDLE_VALIDATE);

export const handleClear = createAction(CONST.HANDLE_CLEAR);