/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import _ from "lodash";

export const name = "AdminRoomPage";

const initialState = freeze({
  rentType: [
    { value: "perHour", label: "Theo giờ" },
    { value: "12Hour", label: "Qua đêm" }
  ],
  typeOptions: [
    { value: "common", label: "Thường" },
    { value: "cold", label: "Máy lạnh" }
  ],
  statusOptions: [
    { value: "used", label: "Đang sử dụng" },
    { value: "unused", label: "Chưa sử dụng" },
    { value: "fixed", label: "Đang bảo trì" }
  ],
  /*---------------*/
  toggleForm: false,
  toggleModal: false,
  toggleConfirmModal: false,
  /*---------------*/
  isErrorRoomName: false,
  isErrorRoomType: false,
  /*---------------*/
  isGetRoomsSuccess: false,
  isProgressing: false,
  isUpdate: false,
  roomType: {},
  roomStatus: {},
  data: {},
  curRoom: {},
  rooms: []
});

export default handleActions(
  {
    [actions.toggleModal]: (state, action) => {
      if (state.isProgressing) {
        return freeze({
          ...state
        });
      } else {
        if (action.payload === "confirmModal") {
          let curRoom = state.curRoom;
          if (_.isEmpty(curRoom.customerIdentity)) {
            return freeze({
              ...state,
              toggleConfirmModal: !state.toggleConfirmModal
            });
          }
        }
        return freeze({
          ...state,
          toggleModal: !state.toggleModal
        });
      }
    },
    [actions.toggleForm]: (state, action) => {
      if (action.payload === "closeForm") {
        return freeze({
          ...state,
          toggleForm: false,
          roomType: {},
          roomStatus: {},
          isErrorRoomName: false,
          isErrorRoomType: false,
          data: {
            name: "",
            status: "",
            type: ""
          },
          isUpdate: false
        });
      }
      if (action.payload === "openForm") {
        return freeze({
          ...state,
          toggleForm: true
        });
      }
      return freeze({
        ...state,
        toggleForm: !state.toggleForm
      });
    },
    /*------
    -------- Handle Room Detail
    */
    [actions.handleRoomDetail]: (state, action) => {
      if (action.payload.type === "editRoom") {
        const statusIndex = state.statusOptions.findIndex(
          item => item.value === action.payload.data.status
        );
        const typeIndex = state.typeOptions.findIndex(
          item => item.value === action.payload.data.type
        );
        return freeze({
          ...state,
          isUpdate: true,
          toggleForm: true,
          data: action.payload.data,
          curRoom: action.payload.data,
          roomStatus: state.statusOptions[statusIndex],
          roomType: state.typeOptions[typeIndex]
        });
      }
      if (action.payload.type === "delRoom") {
        return freeze({
          ...state,
          data: action.payload.data,
          curRoom: action.payload.data
        });
      }

      return freeze({
        ...state,
        curRoom: action.payload.data
      });
    },
    /*------
    -------- Handle Validate
    */
    [actions.handleValidate]: (state, action) => {
      if (action.payload.checkType === "room") {
        if (action.payload.name === "name") {
          return freeze({
            ...state,
            isErrorRoomName: true
          });
        } else if (action.payload.name === "type") {
          return freeze({
            ...state,
            isErrorRoomType: true
          });
        } else {
          return freeze({
            ...state,
            isErrorRoomType: true,
            isErrorRoomName: true
          });
        }
      } else {
        return freeze({
          ...state
        });
      }
    },
    /*------
    -------- Handle Select Change
    */
    [actions.handleSelectChange]: (state, action) => {
      const selectType = action.payload.selectType;
      const statusIndex = state.statusOptions.findIndex(
        item => item.value === action.payload.option.value
      );
      const typeIndex = state.typeOptions.findIndex(
        item => item.value === action.payload.option.value
      );
      return freeze({
        ...state,
        isErrorRoomType: false,
        roomStatus: state.statusOptions[statusIndex],
        roomType: state.typeOptions[typeIndex],
        data: {
          ...state.data,
          [selectType]: action.payload.option.value
        }
      });
    },
    /*------
    -------- Handle Clear
    */
    [actions.handleInputClear]: (state, action) => {
      return freeze({
        ...state,
        toggleForm: false,
        toggleModal: false,
        toggleConfirmModal: false,
        isErrorRoomName: false,
        isErrorRoomType: false,
        isGetRoomsSuccess: false,
        isProgressing: false,
        isUpdate: false,
        roomType: {},
        roomStatus: {},
        data: {},
        curRoom: {},
        rooms: []
      });
    },
    /*------
    -------- Handle Input Clear
    */
    [actions.handleInputClear]: (state, action) => {
      return freeze({
        ...state,
        roomType: {},
        roomStatus: {},
        isErrorRoomName: false,
        isErrorRoomType: false,
        data: {
          name: "",
          status: "",
          type: ""
        }
      });
    },
    /*------
    -------- Handle Input Change
    */
    [actions.handleInputChange]: (state, action) => {
      let event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      if (name === "name") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            isErrorRoomName: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          return freeze({
            ...state,
            isErrorRoomName: false,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
      }

      if (name === "customerName") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            isErrorCustomerName: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          return freeze({
            ...state,
            isErrorCustomerName: false,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
      }

      if (name === "customerIdentity") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            isErrorCustomerIdentity: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          return freeze({
            ...state,
            isErrorCustomerIdentity: false,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
      }

      return freeze({
        ...state,
        data: {
          ...state.data,
          [name]: value
        }
      });
    },
    /*------
    -------- Get All Rooms
    */
    [actions.getAllRooms]: (state, action) => {
      return freeze({
        ...state,
        isGetRoomsSuccess: true
      });
    },
    [actions.getAllRoomsSuccess]: (state, action) => {
      return freeze({
        ...state,
        isGetRoomsSuccess: false,
        rooms: action.payload
      });
    },
    [actions.getAllRoomsFail]: (state, action) => {
      return freeze({
        ...state,
        isGetRoomsSuccess: true
      });
    },
    /*------
    -------- Add Room
    */
    [actions.addNewRoom]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: true
      });
    },
    [actions.addNewRoomSuccess]: (state, action) => {
      let tempt = [...state.rooms];
      tempt.push(action.payload);
      return freeze({
        ...state,
        isProgressing: false,
        rooms: tempt
      });
    },
    [actions.addNewRoomFail]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false
      });
    },
    /*------
    -------- Update Room
    */
    [actions.updateRoom]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false,
        isUpdate: false
      });
    },
    [actions.updateRoomSuccess]: (state, action) => {
      let tempt = [...state.rooms];
      const index = tempt.findIndex(item => item._id === action.payload._id);
      tempt[index] = action.payload;
      return freeze({
        ...state,
        isProgressing: false,
        rooms: tempt
      });
    },
    [actions.updateRoomFail]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false,
        isUpdate: false
      });
    },
    /*------
    -------- Delete Room
    */
    [actions.delRoom]: (state, action) => {
      return freeze({
        ...state,
        toggleConfirmModal: false
      });
    },
    [actions.delRoomSuccess]: (state, action) => {
      let tempt = [...state.rooms];
      const index = tempt.findIndex(item => item._id === state.curRoom._id);
      tempt.splice(index, 1);
      return freeze({
        ...state,
        rooms: tempt,
        curRoom: {},
        data: {}
      });
    }
  },
  initialState
);
