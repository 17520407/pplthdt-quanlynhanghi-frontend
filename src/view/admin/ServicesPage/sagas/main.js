import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import { takeAction } from "services/forkActionSagas";
import * as serviceAPI from "api/admin/services";
import { NotificationManager } from "react-notifications";
// import _ from "lodash";

export function* handleGetAllServices(action) {
  try {
    let res = yield call(serviceAPI.getAllServices, action.payload);
    yield put(actions.getAllServicesSuccess(res.data.data));
  } catch (err) {
    yield put(actions.getAllServicesFail(err));
  }
}

export function* handleAddNewService(action) {
  try {
    let res = yield call(serviceAPI.addNewService, action.payload);
    if (res.data.data === null) {
      yield put(actions.addNewServiceFail());
      NotificationManager.error("Tên dịch vụ đã tồn tại!", null, 2000);
    } else {
      yield put(actions.addNewServiceSuccess(res.data.data));
      NotificationManager.success("Thêm dịch vụ thành công!", null, 2000);
    }
  } catch (err) {
    yield put(actions.addNewServiceFail(err));
  }
}

export function* handleUpdateService(action) {
  try {
    let res = yield call(serviceAPI.updateService, action.payload);
    yield put(actions.updateServiceSuccess(res.data.data));
    NotificationManager.success("Chỉnh sửa dịch vụ thành công!", null, 2000);
  } catch (err) {
    yield put(actions.updateServiceFail(err));
    NotificationManager.error("Chỉnh sửa dịch vụ thất bại!", null, 2000);
  }
}

export function* handleDelService(action) {
  try {
    let res = yield call(serviceAPI.delService, action.payload);
    yield put(actions.delServiceSuccess(res.data));
    NotificationManager.success("Xoá dịch vụ thành công!", null, 2000);
  } catch (err) {
    yield put(actions.delServiceFail(err));
    NotificationManager.error("Xoá dịch vụ thất bại!", null, 2000);
  }
}

/*---------------------------------------------------------------------*/
// Get All Rooms
export function* getAllServices() {
  yield takeAction(actions.getAllServices, handleGetAllServices);
}
// Add new
export function* addNewService() {
  yield takeAction(actions.addNewService, handleAddNewService);
}
// Update room
export function* updateService() {
  yield takeAction(actions.updateService, handleUpdateService);
}
// Del room
export function* delService() {
  yield takeAction(actions.delService, handleDelService);
}
/*---------------------------------------------------------------------*/

export default [

  getAllServices,
  addNewService,
  updateService,
  delService
]
