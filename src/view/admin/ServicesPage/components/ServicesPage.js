import React, { Component } from "react";
import ServiceTopBar from "./ServiceTopBar";
import ServiceForm from "./ServiceForm";
import ServiceList from "./ServiceList";
import ModalConfirm from "./ServiceModalConfirm";

class ServicesPage extends Component {
  render() {
    return (
      <React.Fragment>
        <ServiceTopBar {...this.props} />
        <ServiceForm {...this.props} />
        <ServiceList {...this.props} />
        <ModalConfirm {...this.props} />
      </React.Fragment>
    );
  }
}

export default ServicesPage;
