import React, { Component } from "react";
import { Col, Row, FormGroup, FormFeedback, Input } from "reactstrap";
import _ from "lodash";

class ServiceForm extends Component {
  _handleAddService = () => {
    const { name, quantity, price } = this.props.data;
    const { toggleUpdate } = this.props;
    if (_.isEmpty(name) && _.isEmpty(quantity) && _.isEmpty(price)) {
      this.props.actions.handleValidate();
    } else {
      if (_.isEmpty(name)) {
        this.props.actions.handleValidate("name");
      } else if (quantity <= 0) {
        this.props.actions.handleValidate("quantity");
      } else if (price <= 0) {
        this.props.actions.handleValidate("price");
      } else {
        toggleUpdate
          ? this.props.actions.updateService(this.props.data)
          : this.props.actions.addNewService(this.props.data);
        this.props.actions.handleInputClear();
      }
    }
  };

  _handleCancel = () => {
    this.props.actions.toggleForm("closeForm");
    this.props.actions.handleInputClear();
  };

  render() {
    const {
      toggleForm,
      isErrorServiceQuantity,
      isErrorServicePrice,
      isErrorServiceName
    } = this.props;

    const { name, quantity, price } = this.props.data;
    return (
      <React.Fragment>
        {toggleForm ? (
          <div className="card-box">
            <Row>
              <Col lg="6">
                <FormGroup>
                  <label>Tên dịch vụ</label>
                  <Input
                    type="text"
                    name="name"
                    placeholder="Bánh snack, khăn giấy, nước...."
                    value={name}
                    onChange={e => this.props.actions.handleInputChange(e)}
                    invalid={isErrorServiceName}
                  />
                  <FormFeedback>Bạn chưa nhập tên dịch vụ !</FormFeedback>
                </FormGroup>
                <FormGroup>
                  <label>Giá tiền</label>
                  <Input
                    type="text"
                    name="price"
                    value={price}
                    placeholder="Nhập giá tiền"
                    onChange={e => this.props.actions.handleInputChange(e)}
                    invalid={isErrorServicePrice}
                  />
                  <FormFeedback>
                    Bạn chưa nhập giá tiền hoặc dữ liệu không đúng (Giá tiền
                    phải là số) !
                  </FormFeedback>
                </FormGroup>
              </Col>
              <Col lg="6">
                <FormGroup>
                  <label>Số lượng</label>
                  <Input
                    type="text"
                    name="quantity"
                    value={quantity}
                    placeholder="Nhập số lượng"
                    onChange={e => this.props.actions.handleInputChange(e)}
                    invalid={isErrorServiceQuantity}
                  />
                  <FormFeedback>
                    Bạn chưa nhập số lượng hoặc dữ liệu không đúng (Số lượng
                    phải là số) !
                  </FormFeedback>
                </FormGroup>
                <FormGroup>
                  <div className="float-right m-t-20">
                    <button
                      type="button"
                      className="btn btn-primary btn-rounded w-md waves-effect waves-light"
                      onClick={this._handleAddService}
                    >
                      Lưu
                    </button>
                    &nbsp;
                    <button
                      type="button"
                      className="btn btn-secondary btn-rounded w-md waves-effect waves-light"
                      onClick={this._handleCancel}
                    >
                      Huỷ
                    </button>
                  </div>
                </FormGroup>
              </Col>
            </Row>
          </div>
        ) : null}
      </React.Fragment>
    );
  }
}

export default ServiceForm;
