import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import ServicesPage from "./ServicesPage";
class ServicesPageContainer extends Component {
  UNSAFE_componentWillMount() {
    /*------
    -------- Handle Clear All State Change Route
    */
    this.props.actions.handleClear();
  }
  componentDidMount() {
    this.props.actions.getAllServices();
  }
  render() {
    return (
      <React.Fragment>
        <ServicesPage {...this.props} />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state[name]
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ServicesPageContainer)
);
