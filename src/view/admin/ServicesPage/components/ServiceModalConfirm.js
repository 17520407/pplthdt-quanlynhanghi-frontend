import React from "react";
import { Modal, ModalBody, ModalFooter } from "reactstrap";

const ModalConfirm = props => {
  const { curService } = props;
  return (
    <Modal
      centered
      isOpen={props.toggleModal}
      toggle={() => props.actions.toggleModal()}
    >
      <ModalBody>
        <div className="text-center">
          <i className="mdi mdi-alert-decagram text-danger fs-50" />
          <p>Bạn có chắc chắn muốn xoá dịch vụ này !</p>
        </div>
      </ModalBody>
      <ModalFooter>
        <button
          type="button"
          className="btn btn-danger btn-rounded w-md waves-effect waves-light"
          onClick={() =>
            props.actions.delService({
              _id: curService._id
            })
          }
        >
          Xoá
        </button>
        <button
          type="button"
          className="btn btn-secondary btn-rounded w-md waves-effect waves-light"
          onClick={() => props.actions.toggleModal()}
        >
          Huỷ
        </button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalConfirm;
