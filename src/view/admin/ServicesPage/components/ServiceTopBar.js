import React from "react";
import { Row, Col } from "reactstrap";

const ServiceTopBar = props => {
  return (
    <Row>
      <Col sm="4">
        <button
          type="button"
          className="btn btn-primary btn-rounded w-md waves-effect waves-light m-b-20"
          onClick={() => props.actions.toggleForm()}
        >
          {props.toggleForm ? (
            <i className="mdi mdi-window-close" />
          ) : (
            <span>
              <i className="mdi mdi-food" />
              &nbsp;Thêm dịch vụ
            </span>
          )}
        </button>
      </Col>
      <Col sm="8">
        <div className="project-sort pull-right">
          <div className="project-sort-item">
            <form className="form-inline">
              <div className="form-group">
                <label>Phase :</label>
                <select className="form-control ml-2 form-control-sm">
                  <option>All Projects(6)</option>
                  <option>Complated</option>
                  <option>Progress</option>
                </select>
              </div>
              <div className="form-group">
                <label>Sort :</label>
                <select className="form-control ml-2 form-control-sm">
                  <option>Date</option>
                  <option>Name</option>
                  <option>End date</option>
                  <option>Start Date</option>
                </select>
              </div>
            </form>
          </div>
        </div>
      </Col>
    </Row>
  );
};

export default ServiceTopBar;
