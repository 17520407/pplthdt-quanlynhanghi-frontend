/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

export const toggleForm = createAction(CONST.TOGGLE_FORM);
export const toggleModal = createAction(CONST.TOGGLE_MODAL);

/*---------------------------------------------------------------------*/

export const getAllServices = createAction(CONST.GET_ALL_SERVICES);
export const getAllServicesSuccess = createAction(CONST.GET_ALL_SERVICES_SUCCESS);
export const getAllServicesFail = createAction(CONST.GET_ALL_SERVICES_FAIL);

export const addNewService = createAction(CONST.ADD_NEW_SERVICE);
export const addNewServiceSuccess = createAction(CONST.ADD_NEW_SERVICE_SUCCESS);
export const addNewServiceFail = createAction(CONST.ADD_NEW_SERVICE_FAIL);

export const updateService = createAction(CONST.UPDATE_SERVICE);
export const updateServiceSuccess = createAction(CONST.UPDATE_SERVICE_SUCCESS);
export const updateServiceFail = createAction(CONST.UPDATE_SERVICE_FAIL);

export const delService = createAction(CONST.DEL_SERVICE);
export const delServiceSuccess = createAction(CONST.DEL_SERVICE_SUCCESS);
export const delServiceFail = createAction(CONST.DEL_SERVICE_FAIL);

/*---------------------------------------------------------------------*/

export const handleInputClear = createAction(CONST.HANDLE_INPUT_CLEAR);
export const handleInputChange = createAction(CONST.HANDLE_INPUT_CHANGE);
export const handleSelectChange = createAction(CONST.HANDLE_SELECT_CHANGE);
export const handleServiceDetail = createAction(CONST.HANDLE_SERVICE_DETAIL);
export const handleValidate = createAction(CONST.HANDLE_VALIDATE);

export const handleClear = createAction(CONST.HANDLE_CLEAR);
