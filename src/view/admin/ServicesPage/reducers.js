/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import _ from "lodash";

export const name = "AdminSerivcePage";

const initialState = freeze({
  toggleForm: false,
  toggleModal: false,
  toggleUpdate: false,
  /*---------------*/
  isErrorServicePrice: false,
  isErrorServiceName: false,
  isErrorServiceQuantity: false,
  /*---------------*/
  isGettingService: false,
  data: {},
  curService: {},
  services: []
});

export default handleActions(
  {
    [actions.toggleModal]: (state, action) => {
      return freeze({
        ...state,
        toggleModal: !state.toggleModal,
        curService: action.payload
      });
    },
    [actions.toggleForm]: (state, action) => {
      if (action.payload === "closeForm") {
        return freeze({
          ...state,
          toggleForm: false
        });
      }
      if (action.payload === "openForm") {
        return freeze({
          ...state,
          toggleForm: true
        });
      }
      return freeze({
        ...state,
        toggleForm: !state.toggleForm
      });
    },
    /*------
    -------- Handle Service Detail
    */
    [actions.handleServiceDetail]: (state, action) => {
      return freeze({
        ...state,
        data: action.payload,
        toggleUpdate: true
      });
    },
    /*------
    -------- Handle Validate
    */
    [actions.handleValidate]: (state, action) => {
      if (action.payload === "name") {
        return freeze({
          ...state,
          isErrorServiceName: true
        });
      } else if (action.payload === "price") {
        return freeze({
          ...state,
          isErrorServicePrice: true
        });
      } else if (action.payload === "quantity") {
        return freeze({
          ...state,
          isErrorServiceQuantity: true
        });
      } else {
        return freeze({
          ...state,
          isErrorServicePrice: true,
          isErrorServiceName: true,
          isErrorServiceQuantity: true
        });
      }
    },
    /*------
    -------- Handle Input Clear
    */
    [actions.handleClear]: (state, action) => {
      return freeze({
        ...state,
        toggleForm: false,
        toggleModal: false,
        toggleUpdate: false,
        /*---------------*/
        isErrorServicePrice: false,
        isErrorServiceName: false,
        isErrorServiceQuantity: false,
        /*---------------*/
        isGettingService: false,
        data: {},
        curService: {},
        services: []
      });
    },
    /*------
    -------- Handle Input Clear
    */
    [actions.handleInputClear]: (state, action) => {
      return freeze({
        ...state,
        isErrorServicePrice: false,
        isErrorServiceName: false,
        isErrorServiceQuantity: false,
        data: {
          name: "",
          price: 0,
          quantity: 0
        }
      });
    },
    /*------
    -------- Handle Input Change
    */
    [actions.handleInputChange]: (state, action) => {
      let event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      var numberPattern = new RegExp(/^[0-9]*$/);

      if (name === "name") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            isErrorServiceName: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          return freeze({
            ...state,
            isErrorServiceName: false,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
      }

      if (name === "quantity") {
        if (_.isEmpty(value) || !numberPattern.test(value)) {
          return freeze({
            ...state,
            isErrorServiceQuantity: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          return freeze({
            ...state,
            isErrorServiceQuantity: false,
            data: {
              ...state.data,
              [name]: Number.parseInt(value)
            }
          });
        }
      }

      if (name === "price") {
        if (_.isEmpty(value) || !numberPattern.test(value)) {
          return freeze({
            ...state,
            isErrorServicePrice: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          return freeze({
            ...state,
            isErrorServicePrice: false,
            data: {
              ...state.data,
              [name]: Number.parseInt(value)
            }
          });
        }
      }

      return freeze({
        ...state,
        data: {
          ...state.data,
          [name]: value
        }
      });
    },
    /*------
    -------- Get All Services
    */
    [actions.getAllServices]: (state, action) => {
      return freeze({
        ...state,
        isGettingService: true
      });
    },
    [actions.getAllServicesSuccess]: (state, action) => {
      return freeze({
        ...state,
        isGettingService: false,
        services: action.payload
      });
    },
    [actions.getAllServicesFail]: (state, action) => {
      return freeze({
        ...state,
        isGettingService: false
      });
    },
    /*------
    -------- Add New Service
    */
    [actions.addNewServiceSuccess]: (state, action) => {
      let tempt = [...state.services];
      tempt.push(action.payload);
      return freeze({
        ...state,
        services: tempt
      });
    },
    /*------
    -------- Update Service
    */
    [actions.updateService]: (state, action) => {
      return freeze({
        ...state,
        toggleUpdate: false
      });
    },
    [actions.updateServiceSuccess]: (state, action) => {
      let tempt = [...state.services];
      const index = tempt.findIndex(item => item._id === action.payload._id);
      tempt[index] = action.payload;
      return freeze({
        ...state,
        services: tempt
      });
    },
    /*------
    -------- Delete Service
    */
    [actions.delService]: (state, action) => {
      return freeze({
        ...state,
        toggleModal: false
      });
    },
    [actions.delServiceSuccess]: (state, action) => {
      let tempt = [...state.services];
      const index = tempt.findIndex(item => item._id === state.curService._id);
      tempt.splice(index, 1);
      return freeze({
        ...state,
        services: tempt,
        curService: {}
      });
    }
  },
  initialState
);
