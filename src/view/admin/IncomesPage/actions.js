/**
 * @file All actions will be listed here
 */

import { createAction } from 'redux-actions';
import * as CONST from './constants';


export const toggleModal = createAction(CONST.TOGGLE_MODAL);

export const getIncomes = createAction(CONST.GET_INCOMES);
export const getIncomesSuccess = createAction(CONST.GET_INCOMES_SUCCESS);
export const getIncomesFail = createAction(CONST.GET_INCOMES_FAIL);

export const moneyDraw = createAction(CONST.MONEY_DRAW);
export const moneyDrawSuccess = createAction(CONST.MONEY_DRAW_SUCCESS);
export const moneyDrawFail = createAction(CONST.MONEY_DRAW_FAIL);

export const handleValidate = createAction(CONST.HANDLE_VALIDATE);
export const handleInputChange = createAction(CONST.HANDLE_INPUT_CHANGE);

export const handleClear = createAction(CONST.HANDLE_CLEAR);
