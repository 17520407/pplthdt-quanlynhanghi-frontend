/**
 * @file constants
 */

export const TOGGLE_MODAL = "INCOME_PAGE/TOGGLE_MODAL";

export const GET_INCOMES = "INCOME_PAGE/GET_INCOMES";
export const GET_INCOMES_SUCCESS = "INCOME_PAGE/GET_INCOMES_SUCCESS";
export const GET_INCOMES_FAIL = "INCOME_PAGE/GET_INCOMES_FAIL";

export const MONEY_DRAW = "INCOME_PAGE/MONEY_DRAW";
export const MONEY_DRAW_SUCCESS = "INCOME_PAGE/MONEY_DRAW_SUCCESS";
export const MONEY_DRAW_FAIL = "INCOME_PAGE/MONEY_DRAW_FAIL";

export const HANDLE_VALIDATE = "INCOME_PAGE/HANDLE_VALIDATE";
export const HANDLE_INPUT_CHANGE = "INCOME_PAGE/HANDLE_INPUT_CHANGE";

export const HANDLE_CLEAR = "INCOME_PAGE/HANDLE_CLEAR";

