/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import _ from "lodash";

export const name = "AdminIncomePage";

const initialState = freeze({
  isGettingIncomes: false,
  isMoneyDrawSuccess: false,
  listIncomes: [],
  dashBoard: {},
  toggleDraw: false,
  isErrorTakeMoney: false,
  isOutOfMoney: false,
  data: {}
});

export default handleActions(
  {
    /*------
    -------- Toggle Detail
    */
    [actions.toggleModal]: (state, action) => {
      if (action.payload === "moneyDraw") {
        return freeze({
          ...state,
          toggleDraw: !state.toggleDraw
        });
      }
    },
    [actions.handleClear]: (state, action) => {
      return freeze({
        ...state,
        isGettingIncomes: false,
        isMoneyDrawSuccess: false,
        listIncomes: [],
        dashBoard: {},
        toggleDraw: false,
        toggleDetail: false,
        isErrorTakeMoney: false,
        data: {}
      });
    },
    /*------
    -------- Handle Validate
    */
    [actions.handleValidate]: (state, action) => {
      if (action.payload === "takeMoney") {
        return freeze({
          ...state,
          isErrorTakeMoney: true
        });
      } else {
        return freeze({
          ...state
        });
      }
    },
    [actions.handleInputChange]: (state, action) => {
      let event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      if (name === "takeMoney") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            isErrorTakeMoney: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          if (Number.parseInt(value) > state.dashBoard.totalMoney) {
            return freeze({
              ...state,
              isErrorTakeMoney: true,
              data: {
                ...state.data,
                [name]: value
              }
            });
          } else {
            return freeze({
              ...state,
              isErrorTakeMoney: false,
              data: {
                ...state.data,
                [name]: value
              }
            });
          }
        }
      }

      return freeze({
        ...state,
        data: {
          ...state.data,
          [name]: value
        }
      });
    },
    /*------
    -------- Get Incomes
    */
    [actions.getIncomes]: (state, action) => {
      return freeze({
        ...state,
        isGettingIncomes: true
      });
    },
    [actions.getIncomesSuccess]: (state, action) => {
      if (action.payload.length <= 1) {
        return freeze({
          ...state,
          dashBoard: action.payload[0],
          isGettingIncomes: false,
          isMoneyDrawSuccess: false
        });
      } else {
        let temptDashBorad = action.payload.pop();
        return freeze({
          ...state,
          listIncomes: action.payload,
          dashBoard: temptDashBorad,
          isGettingIncomes: false,
          isMoneyDrawSuccess: false
        });
      }
    },
    [actions.getIncomesFail]: (state, action) => {
      return freeze({
        ...state,
        isGettingIncomes: false
      });
    },
    /*------
    -------- Money Draw
    */
    [actions.moneyDraw]: (state, action) => {
      return freeze({
        ...state,
        isMoneyDrawSuccess: false,
        isProgressing: true
      });
    },
    [actions.moneyDrawSuccess]: (state, action) => {
      return freeze({
        ...state,
        toggleDraw: false,
        isMoneyDrawSuccess: true,
        isErrorTakeMoney: false,
        data: {
          note: "",
          takeMoney: ""
        },
        isProgressing: false
      });
    },
    [actions.moneyDrawFail]: (state, action) => {
      return freeze({
        ...state,
        isMoneyDrawSuccess: false,
        isProgressing: false
      });
    },
  
  },
  initialState
);
