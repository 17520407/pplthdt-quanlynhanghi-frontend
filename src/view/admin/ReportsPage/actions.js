/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

export const getReportMonth = createAction(CONST.GET_REPORT_MONTH);
export const getReportMonthSuccess = createAction(CONST.GET_REPORT_MONTH_SUCCESS);
export const getReportMonthFail = createAction(CONST.GET_REPORT_MONTH_FAIL);

export const downloadReportMonth = createAction(CONST.DOWNLOAD_REPORT_MONTH);
export const downloadReportMonthSuccess = createAction(CONST.DOWNLOAD_REPORT_MONTH_SUCCESS);
export const downloadReportMonthFail = createAction(CONST.DOWNLOAD_REPORT_MONTH_FAIL);

/*-----------------------------------------------------------------------*/ 
export const handleMonthSelect = createAction(CONST.HANDLE_MONTH_SELECT);