import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as reportAPI from "api/admin/report";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "react-notifications";

export function* handleGetReportMonth(action) {
  try {
    let res = yield call(reportAPI.getReportMonth, action.payload);
    if (res.data.success) {
      yield put(actions.getReportMonthSuccess(res.data.data));
    } else {
      yield put(actions.getReportMonthFail(res.data.message));
    }
  } catch (err) {
    yield put(actions.getReportMonthFail(err));
  }
}

export function* handleDownloadReportMonth(action) {
  try {
    let res = yield call(reportAPI.downloadReportMonth, action.payload);
    if (res.data.success) {
      yield put(actions.downloadReportMonthSuccess(res.data.data));
      NotificationManager.success(
        "Lấy đường link báo cáo thành công !",
        "Thông báo",
        1500
      );
      window.open(res.data.data);
    } else {
      yield put(actions.downloadReportMonthFail(res.data.message));
      NotificationManager.error("Tải báo cáo thất bại !", "Lỗi", 2000);
    }
  } catch (err) {
    yield put(actions.downloadReportMonthFail(err));
  }
}

/*---------------------------------------------------------------------*/
//Get Report Month
export function* getReportMonth() {
  yield takeAction(actions.getReportMonth, handleGetReportMonth);
}
//Download Report Month
export function* downloadReportMonth() {
  yield takeAction(actions.downloadReportMonth, handleDownloadReportMonth);
}
/*---------------------------------------------------------------------*/

export default [getReportMonth, downloadReportMonth];
