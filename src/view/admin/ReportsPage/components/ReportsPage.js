import React, { Component } from "react";
import { FormGroup, Label } from "reactstrap";
import DataTable, { memoize } from "react-data-table-component";
import _ from "lodash";
import LoadingScreen from "../../../../modules/libs/LoadingScreen";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import "moment/locale/vi"; // without this line it didn't work
moment.locale("vi");

const columns = memoize(props => [
  {
    name: "Thời gian",
    selector: "time",
    cell: row => <div>{moment(row.time).format("lll")}</div>
  },
  {
    name: "Nhân viên",
    selector: "nameEmployeesInShift",
    sortable: true
  },
  {
    name: "Số tiền thu được",
    selector: "totalMoney",
    cell: row => <div className="text-primary">{row.totalMoney}</div>
  },
  {
    name: "Số tiền đã rút",
    selector: "takeMoney",
    cell: row => <div className="text-danger">{row.takeMoney}</div>
  },
  {
    name: "Số tiền còn lại",
    selector: "currentMoney",
    cell: row => <div className="text-success">{row.currentMoney}</div>
  },
  {
    name: "Ghi chú",
    selector: "note",
    wrap: true,
    cell: row => {
      if (_.isEmpty(row.note)) {
        return <div>Không có ghi chú</div>;
      } else {
        return <div>{row.note}</div>;
      }
    }
  }
]);

class ReportsPage extends Component {
  componentDidMount() {
    var month = new Date();
    this.props.actions.handleMonthSelect(month);
    this.props.actions.getReportMonth({
      month: moment(month).format("M"),
      year: moment().format("YYYY")
    });
  }

  componentDidUpdate(prevState) {
    const { month } = this.props.data;
    const { isSelectedMonthReport } = this.props;
    if (prevState.month !== month && isSelectedMonthReport) {
      if (moment(month).format("M") <= moment().format("M")) {
        this.props.actions.getReportMonth({
          month: moment(month).format("M"),
          year: moment().format("YYYY")
        });
      }
    }
  }

  _handleDownloadReport = () => {
    const { month } = this.props.data;
    let selectedMonth = Number.parseInt(moment(month).format("M"));
    let curYear = Number.parseInt(moment().format("YYYY"));
    this.props.actions.downloadReportMonth({
      month: selectedMonth,
      year: curYear
    });
  };

  render() {
    const {
      isGettingReports,
      reports,
      monthTotalMoney,
      isProgressing
    } = this.props;
    const { month } = this.props.data;

    return (
      <React.Fragment>
        <div className="card-box">
          <div className="d-flex jc-space-between">
            <FormGroup>
              <FormGroup>
                <Label>Thời gian bắt đầu</Label>
                <DatePicker
                  className="date-picker-custom"
                  onChange={month =>
                    this.props.actions.handleMonthSelect(month)
                  }
                  selected={month}
                  dateFormat="MM/yyyy"
                  showMonthYearPicker
                />
              </FormGroup>
              <FormGroup>
                <button
                  type="button"
                  className="btn btn-primary btn-rounded w-md waves-effect waves-light"
                  onClick={this._handleDownloadReport}
                  disabled={isProgressing}
                >
                  {isProgressing ? (
                    <div>
                      <div className="spinner-border spinner-custom text-custom">
                        <span className="sr-only">Loading...</span>
                      </div>
                      &nbsp; Đang xử lí...
                    </div>
                  ) : (
                    <span>
                      <i className="mdi mdi-arrow-collapse-down" />
                      Tải xuống
                    </span>
                  )}
                </button>
              </FormGroup>
            </FormGroup>
            <FormGroup>
              <h3>Tổng số tiền tháng {moment(month).format("M")}</h3>
              <h3 className="text-success">{monthTotalMoney} VNĐ</h3>
            </FormGroup>
          </div>

          {isGettingReports ? (
            <LoadingScreen />
          ) : (
            <DataTable
              title="Báo cáo tháng"
              data={reports}
              columns={columns(this.props)}
              pagination
              highlightOnHover
            />
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default ReportsPage;
