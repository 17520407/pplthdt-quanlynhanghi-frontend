import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as employeeAPI from "api/admin/employee";
import * as shiftAPI from "api/shifts";
import { takeAction } from "services/forkActionSagas";
import { NotificationManager } from "react-notifications";

export function* handleGetAllEmployees(action) {
  try {
    let res = yield call(employeeAPI.getEmployees, action.payload);
    yield put(actions.getAllEmployeesSuccess(res.data.data));
  } catch (err) {
    yield put(actions.getAllEmployeesFail(err));
  }
}

export function* handleChangeShiftByTime(action) {
  try {
    let res = yield call(shiftAPI.getShiftChangeByTime, action.payload);
    if (res.data.success) {
      yield put(actions.getShiftChangeByTimeSuccess(res.data.data));
    } else {
      yield put(actions.getShiftChangeByTimeFail(res.data.message));
    }
  } catch (error) {
    yield put(actions.getShiftChangeByTimeFail(error));
  }
}

export function* handleAddEmployee(action) {
  try {
    let res = yield call(employeeAPI.addEmployee, action.payload);
    yield put(actions.addEmployeeSuccess(res.data.data));
    NotificationManager.success(
      "Thêm nhân viên thành công !",
      "Thông báo",
      2000
    );
  } catch (err) {
    yield put(actions.addEmployeeFail(err));
  }
}

export function* handleUpdateEmployee(action) {
  try {
    let res = yield call(employeeAPI.updateEmployee, action.payload);
    yield put(actions.updateEmployeeSuccess(res.data.data));
    NotificationManager.success(
      "Chỉnh sửa thông tin nhân viên thành công !",
      "Thông báo",
      2000
    );
  } catch (err) {
    yield put(actions.updateEmployeeFail(err));
    NotificationManager.error(
      "Chỉnh sửa nhân viên thất bại, vui lòng kiểm tra lại dữ liệu đầu vào hoặc đường truyền",
      "Thông báo",
      2000
    );
  }
}

export function* handleDelEmployee(action) {
  try {
    let res = yield call(employeeAPI.delEmployee, action.payload);
    yield put(actions.delEmployeeSuccess(res.data.data));
    NotificationManager.success(
      "Xoá nhân viên thành công !",
      "Thông báo",
      2000
    );
  } catch (err) {
    yield put(actions.delEmployeeFail(err));
    NotificationManager.error(
      "Chỉnh sửa nhân viên thất bại, vui lòng kiểm tra lại dữ liệu đầu vào hoặc đường truyền",
      "Thông báo",
      2000
    );
  }
}

export function* handleTakeShift(action) {
  try {
    let res = yield call(employeeAPI.takeShift, action.payload);
    yield put(actions.takeShiftSuccess(res.data.data));
    NotificationManager.success("Giao ca thành công !", "Thông báo", 2000);
  } catch (err) {
    yield put(actions.takeShiftFail(err));
    NotificationManager.error(
      "Giao ca thành công, vui kiểm tra lại",
      "Thông báo",
      2000
    );
  }
}

/*---------------------------------------------------------------------*/
// Add Employee
export function* addEmployee() {
  yield takeAction(actions.addEmployee, handleAddEmployee);
}
// Update Employee
export function* updateEmployee() {
  yield takeAction(actions.updateEmployee, handleUpdateEmployee);
}
// Update Employee
export function* delEmployee() {
  yield takeAction(actions.delEmployee, handleDelEmployee);
}
// Get Employees
export function* getAllEmployees() {
  yield takeAction(actions.getAllEmployees, handleGetAllEmployees);
}
// Employee Take Shift
export function* takeShift() {
  yield takeAction(actions.takeShift, handleTakeShift);
}
// Get Shift Change By Time
export function* getShiftChangeByTime() {
  yield takeAction(actions.getShiftChangeByTime, handleChangeShiftByTime);
}
/*---------------------------------------------------------------------*/

export default [
  addEmployee,
  updateEmployee,
  delEmployee,
  getAllEmployees,
  takeShift,
  getShiftChangeByTime
];
