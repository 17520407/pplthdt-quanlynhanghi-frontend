/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import _ from "lodash";
import moment from "moment";

export const name = "AdminEmployeesPage";

const initialState = freeze({
  toggleCalendarModal: false,
  toggleDelEmployee: false,
  toggleTakeShift: false,
  isProgressing: false,
  isGettingEmployees: false,
  isGettingShiftChange: false,
  isSelectedTimeShiftChange: false,
  activeTab: "1",
  isUpdate: false,
  /*---------------*/
  isErrorName: false,
  isErrorIdentity: false,
  isErrorAddress: false,
  isErrorSalary: false,
  isErrorUsername: false,
  isErrorPassword: false,
  isErrorShifts: false,
  /*---------------*/
  data: {},
  curEmployee: {},
  listEmployees: [],
  shiftsChanging: {},
  beginTime: null,
  endTime: new Date(),
  shifts: [
    {
      label: "Thứ hai",
      day: "monday",
      dayOfWork: "0",
      isErrorShiftStartTime: false,
      isErrorShiftEndTime: false
    },
    {
      label: "Thứ ba",
      day: "tuesday",
      dayOfWork: "1",
      isErrorShiftStartTime: false,
      isErrorShiftEndTime: false
    },
    {
      label: "Thứ tư",
      day: "wednesday",
      dayOfWork: "2",
      isErrorShiftStartTime: false,
      isErrorShiftEndTime: false
    },
    {
      label: "Thứ năm",
      day: "thursday",
      dayOfWork: "3",
      isErrorShiftStartTime: false,
      isErrorShiftEndTime: false
    },
    {
      label: "Thứ sáu",
      day: "friday",
      dayOfWork: "4",
      isErrorShiftStartTime: false,
      isErrorShiftEndTime: false
    },
    {
      label: "Thứ bảy",
      day: "saturday",
      dayOfWork: "5",
      isErrorShiftStartTime: false,
      isErrorShiftEndTime: false
    },
    {
      label: "Chủ nhật",
      day: "sunday",
      dayOfWork: "6",
      isErrorShiftStartTime: false,
      isErrorShiftEndTime: false
    }
  ]
});

export default handleActions(
  {
    [actions.toggleModal]: (state, action) => {
      if (action.payload === "calendarModal") {
        return freeze({
          ...state,
          toggleCalendarModal: !state.toggleCalendarModal
        });
      }
      if (action.payload === "delEmployee") {
        return freeze({
          ...state,
          toggleDelEmployee: !state.toggleDelEmployee
        });
      }

      if (action.payload === "takeShift") {
        return freeze({
          ...state,
          toggleTakeShift: !state.toggleTakeShift
        });
      }
      return freeze({
        ...state
      });
    },
    [actions.handleClearTime]: (state, action) => {
      return freeze({
        ...state,
        isGettingShiftChange: false,
        isSelectedTimeShiftChange: false
      });
    },
    /*------
    -------- Handle Clear
    */
    [actions.handleClear]: (state, action) => {
      return freeze({
        ...state,
        toggleCalendarModal: false,
        toggleDelEmployee: false,
        toggleTakeShift: false,
        isProgressing: false,
        isGettingEmployees: false,
        activeTab: "1",
        isUpdate: false,
        /*---------------*/
        isErrorName: false,
        isErrorIdentity: false,
        isErrorAddress: false,
        isErrorSalary: false,
        isErrorUsername: false,
        isErrorPassword: false,
        isErrorShifts: false,
        /*---------------*/
        data: {},
        curEmployee: {},
        listEmployees: []
      });
    },
    /*------
    -------- Handle Employee Detail
    */
    [actions.handleEmployeeDetail]: (state, action) => {
      if (action.payload.type === "editEmployee") {
        return freeze({
          ...state,
          isUpdate: true,
          curEmployee: action.payload.data,
          data: {
            ...action.payload.data,
            salary: action.payload.data.salary.toString()
          },
          shifts: action.payload.data.shifts,
          activeTab: "1"
        });
      }
      if (action.payload.type === "takeShift") {
        if (action.payload.data.isInShift) {
          return freeze({
            ...state
          });
        } else {
          return freeze({
            ...state,
            curEmployee: action.payload.data,
            toggleTakeShift: true
          });
        }
      }

      return freeze({
        ...state,
        curEmployee: action.payload.data
      });
    },
    /*------
    -------- Handle Change Tab
    */
    [actions.handleChangeTab]: (state, action) => {
      return freeze({
        ...state,
        activeTab: action.payload
      });
    },
    /*------
    -------- Handle Clearfield
    */
    [actions.handleClearField]: (state, action) => {
      let temptShifts = [];
      state.shifts.map(item => {
        temptShifts.push({
          label: item.label,
          day: item.day,
          dayOfWork: item.dayOfWork,
          isErrorShiftStartTime: false,
          isErrorShiftEndTime: false
        });
      });
      return freeze({
        ...state,
        data: {
          name: "",
          identity: "",
          address: "",
          salary: "",
          username: "",
          password: ""
        },
        isUpdate: false,
        shifts: temptShifts,
        isErrorName: false,
        isErrorIdentity: false,
        isErrorAddress: false,
        isErrorSalary: false,
        isErrorUsername: false,
        isErrorPassword: false
      });
    },
    /*------
    -------- Handle Add Shift
    */
    [actions.handleAddCalendar]: (state, action) => {
      const event = action.payload.event;
      const target = event.target;
      const name = target.name;
      const value = target.value;

      let tempt = [...state.shifts];
      const index = tempt.findIndex(item => item.day === action.payload.day);
      if (index !== -1) {
        let curShift = { ...tempt[index], [name]: value };
        if (
          (_.isEmpty(curShift.startTime) && _.isEmpty(curShift.endTime)) ||
          (!_.isEmpty(curShift.startTime) && !_.isEmpty(curShift.endTime))
        ) {
          tempt[index] = {
            ...curShift,
            isErrorShiftStartTime: false,
            isErrorShiftEndTime: false
          };
        } else {
          if (_.isEmpty(curShift.startTime) && !_.isEmpty(curShift.endTime)) {
            tempt[index] = {
              ...curShift,
              isErrorShiftStartTime: true
            };
          } else {
            tempt[index] = {
              ...curShift,
              isErrorShiftEndTime: true
            };
          }
        }
      } else {
        tempt.push({
          day: action.payload.day,
          [name]: value
        });
      }
      return freeze({
        ...state,
        shifts: tempt,
        isErrorShifts: false,
        data: {
          ...state.data,
          shifts: tempt
        }
      });
    },
    /*------
    -------- Handle Time Select
    */
    [actions.handleTimeSelect]: (state, action) => {
      let name = action.payload.type;
      let value = moment(action.payload.data).format("YYYY-MM-DD");
      if (name === "beginTime") {
        return freeze({
          ...state,
          beginTime: action.payload.data,
          data: {
            ...state.data,
            [name]: value
          },
          isSelectedTimeShiftChange: true
        });
      }
      if (name === "endTime") {
        return freeze({
          ...state,
          endTime: action.payload.data,
          data: {
            ...state.data,
            [name]: value
          },
          isSelectedTimeShiftChange: true
        });
      }
      return freeze({
        ...state
      });
    },
    /*------
    -------- Handle Validate
    */
    [actions.handleValidate]: (state, action) => {
      if (action.payload === "name") {
        return freeze({
          ...state,
          isErrorName: true
        });
      } else if (action.payload === "identity") {
        return freeze({
          ...state,
          isErrorIdentity: true
        });
      } else if (action.payload === "address") {
        return freeze({
          ...state,
          isErrorAddress: true
        });
      } else if (action.payload === "salary") {
        return freeze({
          ...state,
          isErrorSalary: true
        });
      } else if (action.payload === "username") {
        return freeze({
          ...state,
          isErrorUsername: true
        });
      } else if (action.payload === "password") {
        return freeze({
          ...state,
          isErrorPassword: true
        });
      } else if (action.payload === "shifts") {
        return freeze({
          ...state,
          isErrorShifts: true
        });
      } else {
        return freeze({
          ...state,
          isErrorName: true,
          isErrorIdentity: true,
          isErrorAddress: true,
          isErrorSalary: true,
          isErrorUsername: true,
          isErrorPassword: true,
          isErrorShifts: true
        });
      }
    },
    /*------
    -------- Handle Input Change
    */
    [actions.handleInputChange]: (state, action) => {
      let event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;

      var numberPattern = new RegExp(/^[0-9]*$/);

      if (name === "name") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            isErrorName: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
        return freeze({
          ...state,
          isErrorName: false,
          data: {
            ...state.data,
            [name]: value
          }
        });
      }

      if (name === "identity") {
        if (
          _.isEmpty(value) ||
          !numberPattern.test(value) ||
          value.length < 9
        ) {
          return freeze({
            ...state,
            isErrorIdentity: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
        return freeze({
          ...state,
          isErrorIdentity: false,
          data: {
            ...state.data,
            [name]: value
          }
        });
      }

      if (name === "address") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            isErrorAddress: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
        return freeze({
          ...state,
          isErrorAddress: false,
          data: {
            ...state.data,
            [name]: value
          }
        });
      }

      if (name === "salary") {
        if (_.isEmpty(value) || !numberPattern.test(value)) {
          return freeze({
            ...state,
            isErrorSalary: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
        return freeze({
          ...state,
          isErrorSalary: false,
          data: {
            ...state.data,
            [name]: value
          }
        });
      }

      if (name === "username") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            isErrorUsername: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
        return freeze({
          ...state,
          isErrorUsername: false,
          data: {
            ...state.data,
            [name]: value
          }
        });
      }

      if (name === "password") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            isErrorPassword: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
        return freeze({
          ...state,
          isErrorPassword: false,
          data: {
            ...state.data,
            [name]: value
          }
        });
      }

      return freeze({
        ...state,
        data: {
          ...state.data,
          [name]: value
        }
      });
    },
    /*------
    -------- Get All Employees
    */
    [actions.getAllEmployees]: (state, action) => {
      return freeze({
        ...state,
        isGettingEmployees: true
      });
    },
    [actions.getAllEmployeesSuccess]: (state, action) => {
      if (action.payload !== null) {
        return freeze({
          ...state,
          listEmployees: action.payload,
          isGettingEmployees: false
        });
      } else {
        return freeze({
          ...state,
          isGettingEmployees: false
        });
      }
    },
    [actions.getAllEmployeesFail]: (state, action) => {
      return freeze({
        ...state,
        isGettingEmployees: false
      });
    },
    /*------
    -------- Add Employee
    */
    [actions.addEmployee]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: true
      });
    },
    [actions.addEmployeeSuccess]: (state, action) => {
      let temptShifts = [];
      state.shifts.map(item => {
        temptShifts.push({
          label: item.label,
          day: item.day,
          dayOfWork: item.dayOfWork,
          isErrorShiftStartTime: false,
          isErrorShiftEndTime: false
        });
      });
      return freeze({
        ...state,
        isProgressing: false,
        data: {
          name: "",
          identity: "",
          salary: "",
          address: "",
          username: "",
          password: ""
        },
        shifts: temptShifts
      });
    },
    [actions.addEmployeeFail]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false
      });
    },
    /*------
    -------- Update Employee
    */
    [actions.updateEmployee]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: true
      });
    },
    [actions.updateEmployeeSuccess]: (state, action) => {
      let temptShifts = [];
      state.shifts.map(item => {
        temptShifts.push({
          label: item.label,
          day: item.day,
          dayOfWork: item.dayOfWork,
          isErrorShiftStartTime: false,
          isErrorShiftEndTime: false
        });
      });
      return freeze({
        ...state,
        isProgressing: false,
        isUpdate: false,
        data: {
          name: "",
          identity: "",
          salary: "",
          address: "",
          username: "",
          password: ""
        },
        shifts: temptShifts
      });
    },
    [actions.updateEmployeeFail]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false
      });
    },
    /*------
    -------- Del Employee
    */
    [actions.delEmployeeSuccess]: (state, action) => {
      let temptListEmployees = [...state.listEmployees];
      const index = temptListEmployees.findIndex(
        item => item._id === state.curEmployee._id
      );
      if (index !== -1) {
        temptListEmployees.splice(index, 1);
      }

      return freeze({
        ...state,
        isProgressing: false,
        listEmployees: temptListEmployees,
        curEmployee: {}
      });
    },
    /*------
    -------- Employee Take Shift
    */
    [actions.takeShiftSuccess]: (state, action) => {
      let temptListEmployees = [...state.listEmployees];

      const curShift = temptListEmployees.findIndex(item => item.isInShift);
      temptListEmployees[curShift] = {
        ...temptListEmployees[curShift],
        isInShift: false
      };
      const index = temptListEmployees.findIndex(
        item => item._id === action.payload.takeShiftId
      );

      temptListEmployees[index] = {
        ...temptListEmployees[index],
        isInShift: true
      };
      return freeze({
        ...state,
        listEmployees: temptListEmployees
      });
    },
    /*------
    -------- Get Employee Shift Changing
    */
    [actions.getShiftChangeByTime]: (state, action) => {
      return freeze({
        ...state,
        isGettingShiftChange: true,
        isSelectedTimeShiftChange: false
      });
    },
    [actions.getShiftChangeByTimeSuccess]: (state, action) => {
      return freeze({
        ...state,
        shiftsChanging: action.payload,
        isGettingShiftChange: false,
        isSelectedTimeShiftChange: false
      });
    },
    [actions.getShiftChangeByTimeFail]: (state, action) => {
      return freeze({
        ...state,
        isGettingShiftChange: false
      });
    }
  },
  initialState
);
