import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import classnames from "classnames";

class EmployeesTopBar extends Component {
  render() {
    const { activeTab } = this.props;
    return (
      <React.Fragment>
        <Row>
          <Col lg="6" md="12" sm="12">
            <button
              type="button"
              className={classnames(
                "btn btn-primary btn-rounded w-md waves-effect waves-light m-b-20",
                { "btn-trans": activeTab !== "1" }
              )}
              onClick={() => this.props.actions.handleChangeTab("1")}
            >
              <i className="mdi mdi-account-plus-outline"></i>
              &nbsp;Thêm nhân viên
            </button>
            &nbsp;
            <button
              type="button"
              className={classnames(
                "btn btn-primary btn-rounded w-md waves-effect waves-light m-b-20",
                { "btn-trans": activeTab !== "2" }
              )}
              onClick={() => this.props.actions.handleChangeTab("2")}
            >
              <i className="mdi mdi-account-group"></i>
              &nbsp;Danh sách nhân viên
            </button>
            &nbsp;
            <button
              type="button"
              className={classnames(
                "btn btn-primary btn-rounded w-md waves-effect waves-light m-b-20",
                { "btn-trans": activeTab !== "3" }
              )}
              onClick={() => this.props.actions.handleChangeTab("3")}
            >
              <i className="mdi mdi-history"></i>
              &nbsp;Lịch sử giao ca
            </button>
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

export default EmployeesTopBar;
