import React, { Component } from "react";
import EmployeesTopBar from "./EmployeesTopBar";
import EmployeesForm from "./EmployeesForm";
import EmployeesShiftsModal from "./EmployeesShiftsModal";
import EmployeesDelModalConfirm from "./EmployeesDelModalConfirm";
import EmployeesList from "./EmployeesList";
import EmployeesTakeShift from "./EmployeesTakeShift";
import EmployeesShiftChanging from "./EmployeesShiftChangeHistory";

class EmployeesPage extends Component {
  render() {
    const { activeTab } = this.props;
    return (
      <React.Fragment>
        <EmployeesTopBar {...this.props} />
        {activeTab === "1" && <EmployeesForm {...this.props} />}
        {activeTab === "2" && <EmployeesList {...this.props} />}
        {activeTab === "3" && <EmployeesShiftChanging {...this.props} />}
        <EmployeesShiftsModal {...this.props} />
        <EmployeesDelModalConfirm {...this.props} />
        <EmployeesTakeShift {...this.props} />
      </React.Fragment>
    );
  }
}

export default EmployeesPage;
