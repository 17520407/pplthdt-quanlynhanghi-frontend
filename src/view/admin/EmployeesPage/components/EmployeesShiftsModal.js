import React from "react";
import { Modal, ModalHeader, ModalBody } from "reactstrap";

import Shift from "./EmployeesShiftItem";

const EmployeesShiftsModal = props => {
  return (
    <Modal
      isOpen={props.toggleCalendarModal}
      toggle={() => props.actions.toggleModal("calendarModal")}
    >
      <ModalHeader toggle={() => props.actions.toggleModal("calendarModal")}>
        Lịch làm của nhân viên
      </ModalHeader>
      <ModalBody>
        {props.shifts.map((shift, index) => {
          return <Shift key={index} shift={shift} actions={props.actions} />;
        })}
      </ModalBody>
    </Modal>
  );
};

export default EmployeesShiftsModal;
