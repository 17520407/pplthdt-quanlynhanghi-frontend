import React from "react";
import { Modal, ModalBody, ModalFooter, Button } from "reactstrap";

const EmployeesDelModalConfirm = props => {
  return (
    <Modal
      isOpen={props.toggleDelEmployee}
      toggle={() => props.actions.toggleModal("delEmployee")}
      centered
    >
      <ModalBody>
        <div className="text-center">
          <i className="mdi mdi-alert-decagram fs-50 text-danger" />
          <p>Bạn có chắc chắn muốn xoá nhân viên này ?</p>
        </div>
      </ModalBody>
      <ModalFooter>
        <Button
          color="danger"
          onClick={() => {
            props.actions.delEmployee({
              _id: props.curEmployee._id
            });
            props.actions.toggleModal("delEmployee");
          }}
        >
          Xoá
        </Button>
        <Button
          color="secondary"
          onClick={() => props.actions.toggleModal("delEmployee")}
        >
          Huỷ
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default EmployeesDelModalConfirm;
