import React from "react";
import { FormGroup, FormFeedback, Input } from "reactstrap";

const ShiftItem = ({ shift, actions }) => {
  return (
    <div className="shift-item">
      <h4 className="text-primary">-- {shift.label}</h4>
      <div className="d-flex c-space-between">
        <FormGroup className="f-basic-50">
          <label>Thời gian bắt đầu</label>
          <Input
            type="time"
            name="startTime"
            onChange={event =>
              actions.handleAddCalendar({
                event,
                day: shift.day
              })
            }
            value={shift.startTime}
            invalid={shift.isErrorShiftStartTime}
          />
          <FormFeedback>
            Hiện tại chưa có thời gian kết thúc ca làm
          </FormFeedback>
        </FormGroup>
        &nbsp;
        <FormGroup className="f-basic-50">
          <label>Thời gian kết thúc</label>
          <Input
            type="time"
            name="endTime"
            onChange={event =>
              actions.handleAddCalendar({
                event,
                day: shift.day
              })
            }
            value={shift.endTime}
            invalid={shift.isErrorShiftEndTime}
          />
          <FormFeedback>
            Hiện tại chưa có thời gian bắt đầu ca làm
          </FormFeedback>
        </FormGroup>
      </div>
    </div>
  );
};

export default ShiftItem;
