import React from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  Button,
  FormGroup,
  Input
} from "reactstrap";
import moment from "moment";

const EmployeesTakeShift = props => {
  return (
    <Modal
      isOpen={props.toggleTakeShift}
      toggle={() => props.actions.toggleModal("takeShift")}
      centered
    >
      <ModalBody>
        <div className="text-center">
          <i className="mdi mdi-comment-question-outline fs-50 text-primary" />
          <h4>Xác nhận giao ca cho nhân viên <strong> {props.curEmployee.name}</strong> ?</h4>
        </div>
        <FormGroup>
          <label>Ghi chú</label>
          <Input
            type="textarea"
            name="note"
            value={props.data.note}
            onChange={(e) => props.actions.handleInputChange(e)}
          />
        </FormGroup>
      </ModalBody>
      <ModalFooter>
        <Button
          color="danger"
          onClick={() => {
            props.actions.takeShift({
              _id: props.curEmployee._id,
              timeTranferShift: moment().format(),
              note: props.data.note 
            });
            props.actions.toggleModal("takeShift");
          }}
        >
          Xác nhận
        </Button>
        <Button
          color="secondary"
          onClick={() => props.actions.toggleModal("takeShift")}
        >
          Huỷ
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default EmployeesTakeShift;
