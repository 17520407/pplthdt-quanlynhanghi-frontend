import React, { Component } from "react";
import DataTable, { memoize } from "react-data-table-component";
import LoadingScreen from "../../../../modules/libs/LoadingScreen";
import { FormGroup, Label } from "reactstrap";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import "moment/locale/vi"; // without this line it didn't work
moment.locale("vi");

const columns = memoize(props => [
  {
    name: "Thời gian",
    selector: "timeTranferShift",
    cell: row => <div>{moment(row.timeTranferShift).format("lll")}</div>
  },
  {
    name: "Người giao ca",
    selector: "tranferShiftName",
    sortable: true
  },
  {
    name: "Người nhận ca",
    selector: "takeShiftName"
  },
  {
    name: "Số tiền lúc giao ca",
    selector: "currentMoney",
    sortable: true,
    cell: row => <div className="text-success">{row.currentMoney} VNĐ</div>
  },
  {
    name: "Ghi chú",
    selector: "note",
    wrap: true
  }
]);

class EmployeesShiftChange extends Component {
  componentDidMount() {
    var beginTime = new Date();
    const { endTime } = this.props;
    beginTime.setDate(beginTime.getDate() - 1);
    this.props.actions.handleTimeSelect({
      type: "beginTime",
      data: beginTime
    });
    this.props.actions.getShiftChangeByTime({
      beginTime: moment(beginTime).format("YYYY-MM-DD"),
      endTime: moment(endTime).format("YYYY-MM-DD")
    });
  }

  componentDidUpdate(prevState) {
    const { endTime, beginTime } = this.props.data;
    const { isSelectedTimeShiftChange } = this.props;
    if (
      (prevState.endTime !== endTime || prevState.beginTime !== beginTime) &&
      isSelectedTimeShiftChange
    ) {
      this.props.actions.getShiftChangeByTime({
        beginTime,
        endTime
      });
    }
  }

  render() {
    const {
      shiftsChanging,
      isGettingShiftChange,
      endTime,
      beginTime
    } = this.props;
    return (
      <React.Fragment>
        <div className="card-box">
          <FormGroup>
            <Label>Thời gian bắt đầu</Label>
            &nbsp;
            <DatePicker
              className="date-picker-custom"
              onChange={beginTime =>
                this.props.actions.handleTimeSelect({
                  type: "beginTime",
                  data: beginTime
                })
              }
              selected={beginTime}
            />
          </FormGroup>
          <FormGroup>
            <Label>Thời gian kết thúc</Label>
            &nbsp;
            <DatePicker
              className="date-picker-custom"
              onChange={endTime =>
                this.props.actions.handleTimeSelect({
                  type: "endTime",
                  data: endTime
                })
              }
              selected={endTime}
            />
          </FormGroup>
          {isGettingShiftChange ? (
            <LoadingScreen />
          ) : (
            <DataTable
              title="Lịch sử giao ca"
              data={shiftsChanging}
              columns={columns(this.props)}
              highlightOnHover
              pagination
            />
          )}
        </div>
      </React.Fragment>
    );
  }
}

export default EmployeesShiftChange;
