/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import _ from "lodash";

export const name = "UserProfilePage";

const initialState = freeze({
  data: {},
  profile: {},
  isErrorOldPassword: false,
  isErrorPassword: false,
  isProgressing: false
});

export default handleActions(
  {
    /*------
    -------- Handle Clear
    */
    [actions.handleClear]: (state, action) => {
      return freeze({
        ...state,
        data: {},
        profile: {},
        isErrorOldPassword: false,
        isErrorPassword: false,
        isProgressing: false
      });
    },
    /*------
    -------- Handle Validate
    */
    [actions.handleValidate]: (state, action) => {
      if (action.payload === "oldPassword") {
        return freeze({
          ...state,
          isErrorOldPassword: true
        });
      }

      if (action.payload === "password") {
        return freeze({
          ...state,
          isErrorPassword: true
        });
      }
      return freeze({
        ...state,
        isErrorOldPassword: true,
        isErrorPassword: true
      });
    },
    /*------
    -------- Handle Input Change
    */
    [actions.handleInputChange]: (state, action) => {
      let event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;

      if (name === "oldPassword") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            isErrorOldPassword: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          return freeze({
            ...state,
            isErrorOldPassword: false,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
      }

      if (name === "password") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            isErrorPassword: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          return freeze({
            ...state,
            isErrorPassword: false,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
      }

      return freeze({
        ...state,
        data: {
          ...state.data,
          [name]: value
        }
      });
    },
    /*------
    -------- Get Userinfo
    */
    [actions.getUserInfoSuccess]: (state, action) => {
      return freeze({
        ...state,
        data: action.payload,
        profile: action.payload
      });
    },
    [actions.getUserInfoFail]: (state, action) => {
      return freeze({
        ...state
      });
    },
    /*------
    -------- Get Userinfo
    */
    [actions.updateUserInfo]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: true
      });
    },
    [actions.updateUserInfoSuccess]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false,
        isErrorOldPassword: false,
        isErrorPassword: false,
        data: {
          ...state.data,
          oldPassword: "",
          password: ""
        }
      });
    },
    [actions.updateUserInfoFail]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false
      });
    }
  },
  initialState
);
