import React, { Component } from "react";
import { FormGroup, FormFeedback, Input, Row, Col, Label } from "reactstrap";
import _ from "lodash";

class ProfileUser extends Component {
  _handleValidate = () => {
    const { oldPassword, password } = this.props.data;
    const { isErrorOldPassword, isErrorPassword } = this.props;
    if (_.isEmpty(oldPassword) && _.isEmpty(password)) {
      this.props.actions.handleValidate();
      return false;
    } else {
      if (_.isEmpty(oldPassword)) {
        this.props.actions.handleValidate("oldPassword");
        return false;
      } else if (_.isEmpty(password)) {
        this.props.actions.handleValidate("password");
        return false;
      } else {
        if (isErrorOldPassword || isErrorPassword) {
          return false;
        } else {
          return true;
        }
      }
    }
  };

  _handleUpdateUserInfo = () => {
    const { oldPassword, password } = this.props.data;
    if (this._handleValidate()) {
      this.props.actions.updateUserInfo({ oldPassword, password });
    }
  };

  render() {
    const { isProgressing, isErrorOldPassword, isErrorPassword } = this.props;
    const {
      name,
      identity,
      address,
      salary,
      oldPassword,
      password
    } = this.props.data;
    return (
      <React.Fragment>
        <div className="card-box">
          <h3>Thông tin nhân viên</h3>
          <Row>
            <Col lg="6" md="12" sm="12">
              <FormGroup>
                <Label>Họ tên</Label>
                <Input type="text" value={name} disabled={true} />
              </FormGroup>
              <FormGroup>
                <Label>Chứng minh nhân dân</Label>
                <Input type="text" value={identity} disabled={true} />
              </FormGroup>
              <FormGroup>
                <Label>Địa chỉ</Label>
                <Input type="text" value={address} disabled={true} />
              </FormGroup>
            </Col>
            <Col lg="6" md="12" sm="12">
              <FormGroup>
                <Label>Lương</Label>
                <Input type="text" value={salary} disabled={true} />
              </FormGroup>
              <FormGroup>
                <Label>Mật khẩu cũ</Label>
                <Input
                  type="password"
                  name="oldPassword"
                  placeholder="Nhập mật khẩu cũ"
                  value={oldPassword}
                  onChange={e => this.props.actions.handleInputChange(e)}
                  disabled={isProgressing}
                  invalid={isErrorOldPassword}
                />
                <FormFeedback>
                  Mật khẩu cũ rỗng hoặc dữ liệu không hợp lệ
                </FormFeedback>
              </FormGroup>
              <FormGroup>
                <Label>Mật khẩu mới</Label>
                <Input
                  type="password"
                  name="password"
                  placeholder="Nhập mật khẩu mới"
                  value={password}
                  onChange={e => this.props.actions.handleInputChange(e)}
                  disabled={isProgressing}
                  invalid={isErrorPassword}
                />
                <FormFeedback>
                  Mật khẩu rỗng hoặc dữ liệu không hợp lệ
                </FormFeedback>
              </FormGroup>
              <button
                type="button"
                className="btn btn-primary btn-rounded w-md waves-effect waves-light float-right"
                onClick={this._handleUpdateUserInfo}
                disabled={isProgressing}
              >
                {isProgressing ? (
                  <div>
                    <div className="spinner-border spinner-custom text-custom">
                      <span className="sr-only">Loading...</span>
                    </div>
                    &nbsp; Đang xử lí...
                  </div>
                ) : (
                  <span>
                    <i className="mdi mdi-arrow-collapse-down" />
                    &nbsp; Lưu thay đổi
                  </span>
                )}
              </button>
            </Col>
          </Row>
        </div>
      </React.Fragment>
    );
  }
}

export default ProfileUser;
