import React, { Component } from "react";
import ProfileUser from "./ProfileUser";
import ProfileShift from "./ProfileShift";

class ProfilePage extends Component {
  render() {
    return (
      <React.Fragment>
        <ProfileUser {...this.props} />
        <ProfileShift {...this.props} />
      </React.Fragment>
    );
  }
}

export default ProfilePage;
