import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import ProfilePage from "./ProfilePage";

class ProfilePageContainer extends Component {
  UNSAFE_componentWillMount() {
    this.props.actions.handleClear();
  }

  componentDidMount() {
    this.props.actions.getUserInfo();
  }
  render() {
    return (
      <React.Fragment>
        <ProfilePage {...this.props} />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state[name]
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ProfilePageContainer)
);
