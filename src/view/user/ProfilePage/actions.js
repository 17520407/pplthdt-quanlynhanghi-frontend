/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

export const toggleForm = createAction(CONST.TOGGLE_FORM);

/*---------------------------------------------------------------------*/
export const getUserInfo = createAction(CONST.GET_USER_INFO);
export const getUserInfoSuccess = createAction(CONST.GET_USER_INFO_SUCCESS);
export const getUserInfoFail = createAction(CONST.GET_USER_INFO_FAIL);

export const updateUserInfo = createAction(CONST.UPDATE_USER_INFO);
export const updateUserInfoSuccess = createAction(CONST.UPDATE_USER_INFO_SUCCESS);
export const updateUserInfoFail = createAction(CONST.UPDATE_USER_INFO_FAIL);

/*---------------------------------------------------------------------*/

export const handleInputChange = createAction(CONST.HANDLE_INPUT_CHANGE);
export const handleValidate = createAction(CONST.HANDLE_VALIDATE);

export const handleClear = createAction(CONST.HANDLE_CLEAR);