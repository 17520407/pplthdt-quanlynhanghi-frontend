import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import { takeAction } from "services/forkActionSagas";
import * as roomAPI from "api/user/rooms";
import * as billAPI from "api/user/bills";
import * as shiftAPI from "api/shifts";
import * as servicesAPI from "api/user/services";
import { NotificationManager } from "react-notifications";
// import _ from "lodash";

export function* handleGetAllRooms(action) {
  try {
    let res = yield call(roomAPI.getAllRooms);
    if (res.status === 403) {
      yield put(actions.getAllRoomsFail(res));
    } else {
      yield put(actions.getAllRoomsSuccess(res.data.data));
    }
  } catch (err) {
    yield put(actions.getAllRoomsFail(err));
  }
}

export function* handleGetAllServices(action) {
  try {
    let res = yield call(servicesAPI.getAllServices);
    if (res.status === 403) {
      yield put(actions.getAllServicesFail(res));
    } else {
      yield put(actions.getAllServicesSuccess(res.data.data));
    }
  } catch (err) {
    yield put(actions.getAllServicesFail(err));
  }
}

/*---------------------------------------------------------------------*/

export function* handleGetBillById(action) {
  try {
    let res = yield call(billAPI.getBillById, action.payload);
    if (res.data.success) {
      yield put(actions.getBillByIdSuccess(res.data.data));
    } else {
      yield put(actions.getBillByIdFail(res.data.message));
    }
  } catch (err) {
    yield put(actions.getBillByIdFail(err));
  }
}

export function* handleShiftChange(action) {
  try {
    let res = yield call(shiftAPI.shiftChange, action.payload);
    if (res.data.success) {
      yield put(actions.shiftChangeSuccess(res.data.data));
      NotificationManager.success("Giao ca thành công !", "Thông báo", 1500);
    } else {
      yield put(actions.shiftChangeFail(res.data.message));
      NotificationManager.error("Giao ca thất bại !", "Lỗi", 2000);
    }
  } catch (err) {
    yield put(actions.shiftChangeFail(err));
  }
}

export function* handleRoomChange(action) {
  try {
    let res = yield call(billAPI.roomChange, action.payload);
    if (res.data.success) {
      yield put(actions.roomChangeSuccess(res.data.data));
      NotificationManager.success(
        "Chuyển phòng thành công !",
        "Thông báo",
        1500
      );
    } else {
      yield put(actions.roomChangeFail(res.data.message));
      NotificationManager.error("Chuyển phòng thất bại !", "Lỗi", 2000);
    }
  } catch (err) {
    yield put(actions.roomChangeFail(err));
  }
}

export function* handleCreateBill(action) {
  try {
    let res = yield call(billAPI.createBill, action.payload);
    if (res.data.success) {
      yield put(actions.createBillSuccess(res.data.data));
      NotificationManager.success("Tạo bill thành công !", "Thông báo", 1500);
    } else {
      yield put(actions.createBillFail(res.data.message));
      NotificationManager.error("Tạo bill thất bại !", "Lỗi", 2000);
    }
  } catch (err) {
    yield put(actions.createBillFail(err));
  }
}

export function* handlePayTheBill(action) {
  try {
    let res = yield call(billAPI.payTheBill, action.payload);
    if (res.data.success) {
      yield put(actions.payTheBillSuccess(res.data.data));
      NotificationManager.success("Thanh toán thành công !", "Thông báo", 1500);
    } else {
      yield put(actions.payTheBillFail(res.data.message));
      NotificationManager.error("Thanh toán thất bại !", "Lỗi", 2000);
    }
  } catch (err) {
    yield put(actions.payTheBillFail(err));
  }
}

export function* handleAddService(action) {
  try {
    let res = yield call(billAPI.addService, action.payload);
    if (res.data.success) {
      yield put(actions.addServiceSuccess(res.data.data));
      NotificationManager.success(
        "Thêm dịch vụ thành công !",
        "Thông báo",
        1500
      );
    } else {
      yield put(actions.addServiceFail(res.data.message));
      NotificationManager.error("Thêm dịch vụ thất bại !", "Lỗi", 2000);
    }
  } catch (err) {
    yield put(actions.addServiceFail(err));
  }
}

export function* handleDelService(action) {
  try {
    let res = yield call(billAPI.delService, action.payload);
    if (res.data.success) {
      yield put(actions.delServiceSuccess(res.data.data));
      NotificationManager.success(
        "Xoá dịch vụ thành công !",
        "Thông báo",
        1500
      );
    } else {
      yield put(actions.delServiceFail(res.data.message));
      NotificationManager.error("Xoá dịch vụ thất bại !", "Lỗi", 2000);
    }
  } catch (err) {
    yield put(actions.delServiceFail(err));
  }
}
/*---------------------------------------------------------------------*/
// Get All Rooms
export function* getAllRooms() {
  yield takeAction(actions.getAllRooms, handleGetAllRooms);
}
// Create bill
export function* createBill() {
  yield takeAction(actions.createBill, handleCreateBill);
}
// Pay the bill
export function* payTheBill() {
  yield takeAction(actions.payTheBill, handlePayTheBill);
}
// Add service
export function* addService() {
  yield takeAction(actions.addService, handleAddService);
}
// Del service
export function* delService() {
  yield takeAction(actions.delService, handleDelService);
}
// Get All Services
export function* getAllServices() {
  yield takeAction(actions.getAllServices, handleGetAllServices);
}
// Get Bill By Id
export function* getBillById() {
  yield takeAction(actions.getBillById, handleGetBillById);
}
// Change Room
export function* roomChange() {
  yield takeAction(actions.roomChange, handleRoomChange);
}
// Change Shift
export function* shiftChange() {
  yield takeAction(actions.shiftChange, handleShiftChange);
}
////////////////////////////////////////////////////////////

export default [
  getAllRooms,
  getBillById,
  createBill,
  payTheBill,
  addService,
  delService,
  getAllServices,
  roomChange,
  shiftChange
];
