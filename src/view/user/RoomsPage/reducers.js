/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";
import _ from "lodash";

export const name = "UserRoomPage";

const initialState = freeze({
  rentType: [
    { value: "perHour", label: "Theo giờ" },
    { value: "12Hour", label: "Qua đêm" }
  ],
  rentTypeSelected: {},
  serviceOptions: [],
  serviceSelected: {},
  roomOptions: [],
  roomSelected: {},
  /*----------------------*/
  toggleUpdate: false,
  toggleForm: false,
  toggleModal: false,
  toggleCustomerModal: false,
  toggleFeeModal: false,
  toggleConfirmModal: false,
  toggleServiceModal: false,
  toggleRoomChangeModal: false,
  toggleShiftChangeModal: false,
  /*----------------------*/
  isErrorCustomerName: false,
  isErrorCustomerIdentity: false,
  isErrorCustomerRentType: false,
  isErrorServiceQuantity: false,
  isErrorServiceName: false,
  isErrorRoomChange: false,
  isErrorTakeShiftUsername: false,
  isErrorTakeShiftPassword: false,
  isNotOwnShift: false,
  /*----------------------*/
  isLoading: false,
  isLoadingBill: false,
  isChangedRoom: false,
  isAddCustomer: false,
  isUpdate: false,
  isProgressing: false,
  data: {},
  curRoom: {},
  rooms: [],
  curService: {},
  services: []
});

export default handleActions(
  {
    [actions.toggleModal]: (state, action) => {
      if (state.isProgressing) {
        return freeze({
          ...state
        });
      } else {
        if (action.payload === "customerModal") {
          return freeze({
            ...state,
            toggleCustomerModal: !state.toggleCustomerModal
          });
        }
        if (action.payload === "feeModal") {
          return freeze({
            ...state,
            toggleFeeModal: !state.toggleFeeModal
          });
        }
        if (action.payload === "confirmModal") {
          return freeze({
            ...state,
            toggleConfirmModal: !state.toggleConfirmModal
          });
        }
        if (action.payload === "serviceModal") {
          return freeze({
            ...state,
            toggleServiceModal: !state.toggleServiceModal
          });
        }
        if (action.payload === "roomChangeModal") {
          return freeze({
            ...state,
            toggleRoomChangeModal: !state.toggleRoomChangeModal
          });
        }
        if (action.payload === "shiftChangeModal") {
          return freeze({
            ...state,
            toggleShiftChangeModal: !state.toggleShiftChangeModal
          });
        }
        return freeze({
          ...state,
          toggleModal: !state.toggleModal
        });
      }
    },
    [actions.toggleForm]: (state, action) => {
      if (action.payload === "closeForm") {
        return freeze({
          ...state,
          toggleForm: false
        });
      }
      if (action.payload === "openForm") {
        return freeze({
          ...state,
          toggleForm: true
        });
      }
      return freeze({
        ...state,
        toggleForm: !state.toggleForm
      });
    },
    /*------
    -------- Handle Clear
    */
    [actions.handleClear]: (state, action) => {
      return freeze({
        ...state,
        rentTypeSelected: {},
        serviceOptions: [],
        serviceSelected: {},
        roomOptions: [],
        roomSelected: {},
        /*----------------------*/
        toggleUpdate: false,
        toggleForm: false,
        toggleModal: false,
        toggleCustomerModal: false,
        toggleFeeModal: false,
        toggleConfirmModal: false,
        toggleServiceModal: false,
        toggleRoomChangeModal: false,
        toggleShiftChangeModal: false,
        /*----------------------*/
        isErrorCustomerName: false,
        isErrorCustomerIdentity: false,
        isErrorCustomerRentType: false,
        isErrorServiceQuantity: false,
        isErrorServiceName: false,
        isErrorRoomChange: false,
        isErrorTakeShiftUsername: false,
        isErrorTakeShiftPassword: false,
        isNotOwnShift: false,
        /*----------------------*/
        isLoading: false,
        isLoadingBill: false,
        isChangedRoom: false,
        isAddCustomer: false,
        isUpdate: false,
        isProgressing: false,
        data: {},
        curRoom: {},
        rooms: [],
        curService: {},
        services: []
      });
    },

    /*------
    -------- Handle Del Service
    */
    [actions.handleDelService]: (state, action) => {
      let temptServices = [...state.services];
      let temptServiceOptions = [...state.serviceOptions];
      const index = temptServices.findIndex(
        item => item.serviceName === action.payload.serviceName
      );
      const indexOption = temptServiceOptions.findIndex(
        item => item.value === action.payload.serviceName
      );

      if (index !== -1) {
        if (indexOption !== -1) {
          let newQuanity =
            Number.parseInt(temptServiceOptions[indexOption].quantity) +
            Number.parseInt(temptServices[index].quantity);
          temptServiceOptions[index] = {
            ...temptServiceOptions[index],
            quantity: newQuanity
          };
          temptServices.splice(index, 1);
        } else {
          temptServiceOptions.push({
            ...temptServices[index],
            value: temptServices[index].serviceName,
            label: temptServices[index].serviceName
          });
          temptServices.splice(index, 1);
        }
      }

      return freeze({
        ...state,
        serviceOptions: temptServiceOptions,
        services: temptServices
      });
    },
    /*------
    -------- Handle Add Service
    */
    [actions.handleAddService]: (state, action) => {
      let temptServices = [...state.services];
      let temptServiceOptions = [...state.serviceOptions];
      const index = temptServices.findIndex(
        item => item.serviceName === state.data.serviceName
      );

      const indexOption = temptServiceOptions.findIndex(
        item => item.value === state.data.serviceName
      );

      if (index === -1) {
        temptServices.push({
          price: state.data.price,
          quantity: state.data.quantity,
          serviceName: state.data.serviceName,
          totalPrice: state.data.totalPrice
        });
      } else {
        let newQuanity =
          Number.parseInt(temptServices[index].quantity) +
          Number.parseInt(state.data.quantity);
        let newTotalPrice =
          temptServices[index].totalPrice + state.data.totalPrice;
        temptServices[index] = {
          ...temptServices[index],
          quantity: newQuanity.toString(),
          totalPrice: newTotalPrice
        };
      }

      if (indexOption !== -1) {
        let newQuanity =
          Number.parseInt(temptServiceOptions[indexOption].quantity) -
          Number.parseInt(state.data.quantity);
        if (newQuanity <= 0) {
          temptServiceOptions.splice(indexOption, 1);
        } else {
          temptServiceOptions[indexOption] = {
            ...temptServiceOptions[indexOption],
            quantity: newQuanity
          };
        }
      }
      return freeze({
        ...state,
        services: temptServices,
        serviceSelected: {},
        serviceOptions: temptServiceOptions,
        data: {
          ...state.data,
          serviceName: "",
          quantity: "",
          price: "",
          totalPrice: ""
        }
      });
    },
    /*------
    -------- Handle Room Detail
    */
    [actions.handleRoomDetail]: (state, action) => {
      if (action.payload.type === "edit") {
        const index = state.rentType.findIndex(
          item => item.value === action.payload.data.rentType
        );
        let curRoomServices = action.payload.data.services;
        let newCurRoomServices = [];
        curRoomServices.map(item => {
          let totalPrice = item.price * item.quantity;
          newCurRoomServices.push({
            ...item,
            totalPrice
          });
        });
        return freeze({
          ...state,
          rentTypeSelected: state.rentType[index],
          services: newCurRoomServices,
          data: {
            customerIdentity: action.payload.data.customerIdentity,
            customerName: action.payload.data.customerName
          },
          isUpdate: true,
          curRoom: action.payload.data
        });
      }
      return freeze({
        ...state,
        curRoom: action.payload.data
      });
    },
    /*------
    -------- Handle Validate
    */
    [actions.handleValidate]: (state, action) => {
      if (action.payload.checkType === "bill") {
        if (action.payload.name === "customerName") {
          return freeze({
            ...state,
            isErrorCustomerName: true
          });
        } else if (action.payload.name === "customerIdentity") {
          return freeze({
            ...state,
            isErrorCustomerIdentity: true
          });
        } else if (action.payload.name === "rentType") {
          return freeze({
            ...state,
            isErrorCustomerRentType: true
          });
        } else {
          return freeze({
            ...state,
            isErrorCustomerName: true,
            isErrorCustomerIdentity: true,
            isErrorCustomerRentType: true
          });
        }
      } else if (action.payload.checkType === "service") {
        if (action.payload.name === "quantity") {
          return freeze({
            ...state,
            isErrorServiceQuantity: true
          });
        } else if (action.payload.name === "serviceName") {
          return freeze({
            ...state,
            isErrorServiceName: true
          });
        } else {
          return freeze({
            ...state,
            isErrorServiceQuantity: true,
            isErrorServiceName: true
          });
        }
      } else if (action.payload.checkType === "roomChange") {
        return freeze({
          ...state,
          isErrorRoomChange: true
        });
      } else if (action.payload.checkType === "shiftChange") {
        if (action.payload.name === "takeShiftUsername") {
          return freeze({
            ...state,
            isErrorTakeShiftUsername: true
          });
        }
        if (action.payload.name === "takeShiftPassword") {
          return freeze({
            ...state,
            isErrorTakeShiftPassword: true
          });
        }
        return freeze({
          ...state,
          isErrorTakeShiftUsername: true,
          isErrorTakeShiftPassword: true
        });
      } else {
        return freeze({
          ...state
        });
      }
    },
    /*------
    -------- Handle Room Change
    */
    [actions.handleRoomChange]: (state, action) => {
      let temptRooms = [...state.rooms];
      let temptRoomOptions = [];
      const index = temptRooms.findIndex(
        item => item._id === action.payload._id
      );
      temptRooms.splice(index, 1);
      temptRooms.map(item => {
        if (item.status === "unused") {
          temptRoomOptions.push({
            value: item._id,
            label: item.name
          });
        }
      });
      return freeze({
        ...state,
        roomOptions: temptRoomOptions
      });
    },
    /*------
    -------- Handle Select Change
    */
    [actions.handleSelectChange]: (state, action) => {
      const selectType = action.payload.selectType;
      if (action.payload.selectType === "serviceName") {
        let totalPrice;
        if (!_.isEmpty(state.data.quantity)) {
          totalPrice =
            Number.parseInt(state.data.quantity) *
            Number.parseInt(action.payload.option.price);
        } else {
          totalPrice = 0;
        }
        return freeze({
          ...state,
          isErrorCustomerRentType: false,
          isErrorServiceName: false,
          serviceSelected: action.payload.option,
          data: {
            ...state.data,
            [selectType]: action.payload.option.value,
            price: action.payload.option.price,
            totalPrice
          }
        });
      } else if (action.payload.selectType === "roomChange") {
        return freeze({
          ...state,
          isErrorRoomChange: false,
          roomSelected: action.payload.option
        });
      } else {
        return freeze({
          ...state,
          isErrorCustomerRentType: false,
          rentTypeSelected: action.payload.option,
          data: {
            ...state.data,
            [selectType]: action.payload.option.value
          }
        });
      }
    },
    /*------
    -------- Handle Input Clear
    */
    [actions.handleInputClear]: (state, action) => {
      if (action.payload === "rentType") {
        return freeze({
          ...state,
          rentTypeSelected: {},
          isUpdate: false,
          data: {
            customerIdentity: "",
            customerName: ""
          },
          isErrorCustomerName: false,
          isErrorCustomerIdentity: false,
          isErrorCustomerRentType: false
        });
      }
      if (action.payload === "service") {
        return freeze({
          ...state,
          data: {
            serviceName: "",
            quantity: "",
            price: "",
            totalPrice: "",
            customerIdentity: "",
            customerName: ""
          },
          rentTypeSelected: {},
          isErrorServiceQuantity: false,
          isErrorServiceName: false,
          serviceSelected: {},
          isAddCustomer: false,
          services: []
        });
      }
      if (action.payload === "roomChange") {
        return freeze({
          ...state,
          isErrorRoomChange: false,
          isChangedRoom: false,
          roomSelected: {}
        });
      }
      if (action.payload === "shiftChange") {
        return freeze({
          ...state,
          data: {
            ...state.data,
            takeShiftUsername: "",
            takeShiftPassword: "",
            timeTranferShift: ""
          },
          isErrorTakeShiftUsername: false,
          isErrorTakeShiftPassword: false
        });
      }
      return freeze({
        ...state
      });
    },
    /*------
    -------- Handle Input Change
    */
    [actions.handleInputChange]: (state, action) => {
      let event = action.payload;
      const target = event.target;
      const value = target.type === "checkbox" ? target.checked : target.value;
      const name = target.name;
      var numberPattern = new RegExp(/^[0-9]*$/);

      if (name === "name") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            isErrorRoomName: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          return freeze({
            ...state,
            isErrorRoomName: false,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
      }

      if (name === "customerName") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            isErrorCustomerName: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          return freeze({
            ...state,
            isErrorCustomerName: false,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
      }

      if (name === "customerIdentity") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            isErrorCustomerIdentity: true,
            data: {
              ...state.data,
              [name]: value
            }
          });
        } else {
          return freeze({
            ...state,
            isErrorCustomerIdentity: false,
            data: {
              ...state.data,
              [name]: value
            }
          });
        }
      }

      if (name === "quantity") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            data: {
              ...state.data,
              [name]: value,
              totalPrice: ""
            },
            isErrorServiceQuantity: true
          });
        } else {
          if (numberPattern.test(value)) {
            if (Number.parseInt(value) > state.serviceSelected.quantity) {
              return freeze({
                ...state,
                data: {
                  ...state.data,
                  [name]: value
                },
                isErrorServiceQuantity: true
              });
            } else {
              let totalPrice;
              if (!_.isEmpty(state.serviceSelected)) {
                totalPrice =
                  Number.parseInt(value) * state.serviceSelected.price;
              } else {
                totalPrice = "Vui lòng chọn dịch vụ";
              }
              return freeze({
                ...state,
                data: {
                  ...state.data,
                  [name]: value,
                  totalPrice
                },
                isErrorServiceQuantity: false
              });
            }
          } else {
            return freeze({
              ...state,
              data: {
                ...state.data,
                [name]: value
              },
              isErrorServiceQuantity: true
            });
          }
        }
      }

      if (name === "takeShiftUsername") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            data: {
              ...state.data,
              [name]: value
            },
            isErrorTakeShiftUsername: true
          });
        } else {
          return freeze({
            ...state,
            data: {
              ...state.data,
              [name]: value
            },
            isErrorTakeShiftUsername: false
          });
        }
      }

      if (name === "takeShiftPassword") {
        if (_.isEmpty(value)) {
          return freeze({
            ...state,
            data: {
              ...state.data,
              [name]: value
            },
            isErrorTakeShiftPassword: true
          });
        } else {
          return freeze({
            ...state,
            data: {
              ...state.data,
              [name]: value
            },
            isErrorTakeShiftPassword: false
          });
        }
      }

      return freeze({
        ...state,
        data: {
          ...state.data,
          [name]: value
        }
      });
    },
    /*------
    -------- Get All Rooms
    */
    [actions.getAllRooms]: (state, action) => {
      return freeze({
        ...state,
        isLoading: true
      });
    },
    [actions.getAllRoomsSuccess]: (state, action) => {
      return freeze({
        ...state,
        isLoading: false,
        rooms: action.payload
      });
    },
    [actions.getAllRoomsFail]: (state, action) => {
      if (action.payload.status === 403) {
        return freeze({
          ...state,
          isLoading: false,
          isNotOwnShift: true
        });
      } else {
        return freeze({
          ...state,
          isLoading: false,
          isNotOwnShift: false
        });
      }
    },
    /*------
    -------- Get Services
    */
    [actions.getAllServicesSuccess]: (state, action) => {
      let tempt = [];
      action.payload.map(item => {
        if (item.quantity > 0) {
          tempt.push({
            label: item.name,
            value: item.name,
            price: item.price,
            quantity: item.quantity
          });
        }
      });
      return freeze({
        ...state,
        serviceOptions: tempt
      });
    },
    [actions.getAllServicesFail]: (state, action) => {
      if (action.payload.status === 403) {
        return freeze({
          ...state,
          isNotOwnShift: true
        });
      }
      return freeze({
        ...state
      });
    },
    /*------
    -------- Create Bill
    */
    [actions.createBill]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: true,
        isAddCustomer: false
      });
    },
    [actions.createBillSuccess]: (state, action) => {
      let temptRooms = [...state.rooms];
      const index = temptRooms.findIndex(
        item => item._id === action.payload.roomId
      );
      if (index !== -1) {
        temptRooms[index] = {
          _id: action.payload.roomId,
          billId: action.payload._id,
          checkinDate: action.payload.checkinDate,
          customerIdentity: action.payload.customerIdentity,
          customerName: action.payload.customerName,
          name: action.payload.roomName,
          rentType: action.payload.rentType,
          services: action.payload.services,
          type: action.payload.roomType,
          status: temptRooms[index].status
        };
      }

      return freeze({
        ...state,
        rooms: temptRooms,
        toggleCustomerModal: false,
        curRoom: action.payload,
        isProgressing: false,
        isAddCustomer: true
      });
    },
    [actions.createBillFail]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false,
        isAddCustomer: false
      });
    },
    /*------
    -------- Add Serivce
    */
    [actions.addService]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: true
      });
    },
    [actions.addServiceSuccess]: (state, action) => {
      let temptRooms = [...state.rooms];
      const index = temptRooms.findIndex(
        item => item._id === action.payload.roomId
      );

      if (index !== -1) {
        temptRooms[index] = {
          _id: action.payload.roomId,
          billId: action.payload._id,
          checkinDate: action.payload.checkinDate,
          customerIdentity: action.payload.customerIdentity,
          customerName: action.payload.customerName,
          name: action.payload.roomName,
          rentType: action.payload.rentType,
          services: action.payload.services,
          type: action.payload.roomType,
          status: temptRooms[index].status
        };
      }
      return freeze({
        ...state,
        isProgressing: false,
        rooms: temptRooms
      });
    },
    [actions.addServiceFail]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false
      });
    },
    /*------
    -------- Del Serivce
    */
    [actions.delService]: (state, action) => {
      return freeze({
        ...state
      });
    },
    [actions.delServiceSuccess]: (state, action) => {
      let temptRooms = [...state.rooms];
      const indexRoom = temptRooms.findIndex(
        item => item.billId === state.curRoom.billId
      );
      let newServices = [];
      action.payload.services.map(item => {
        let totalPrice = item.quantity * item.price;
        newServices.push({
          ...item,
          totalPrice
        });
      });
      temptRooms[indexRoom] = {
        ...temptRooms[indexRoom],
        services: newServices
      };
      return freeze({
        ...state,
        rooms: temptRooms
      });
    },
    [actions.delServiceFail]: (state, action) => {
      return freeze({
        ...state
      });
    },
    /*------
    -------- Get Bill By Id
    */
    [actions.getBillById]: (state, action) => {
      return freeze({
        ...state,
        isLoadingBill: true
      });
    },
    [actions.getBillByIdSuccess]: (state, action) => {
      return freeze({
        ...state,
        isLoadingBill: false,
        curRoom: action.payload
      });
    },
    [actions.getBillByIdFail]: (state, action) => {
      return freeze({
        ...state,
        isLoadingBill: false
      });
    },
    /*------
    -------- Room Change
    */
    [actions.roomChange]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: true,
        isChangedRoom: false
      });
    },
    [actions.roomChangeSuccess]: (state, action) => {
      return freeze({
        ...state,
        isChangedRoom: true,
        isProgressing: false,
        toggleRoomChangeModal: false,
        roomSelected: {},
        isErrorRoomChange: false
      });
    },
    [actions.roomChangeFail]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false,
        isChangedRoom: false
      });
    },
    /*------
    -------- Shift Change
    */
    [actions.shiftChange]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: true
      });
    },
    [actions.shiftChangeSuccess]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false,
        isNotOwnShift: true
      });
    },
    [actions.shiftChangeFail]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false
      });
    },
    /*------
    -------- Pay Bill
    */
    [actions.payTheBill]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: true
      });
    },
    [actions.payTheBillSuccess]: (state, action) => {
      let temptRooms = [...state.rooms];
      const index = temptRooms.findIndex(
        item => item._id === action.payload.roomId
      );

      temptRooms[index] = {
        name: temptRooms[index].name,
        type: temptRooms[index].type,
        status: "unused",
        _id: temptRooms[index]._id
      };

      return freeze({
        ...state,
        rooms: temptRooms,
        isProgressing: false,
        toggleFeeModal: false
      });
    },
    [actions.payTheBillFail]: (state, action) => {
      return freeze({
        ...state,
        isProgressing: false
      });
    }
  },
  initialState
);
