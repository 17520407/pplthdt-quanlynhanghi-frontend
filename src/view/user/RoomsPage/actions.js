/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

export const toggleForm = createAction(CONST.TOGGLE_FORM);
export const toggleModal = createAction(CONST.TOGGLE_MODAL);

/*---------------------------------------------------------------------*/

export const getAllRooms = createAction(CONST.GET_ALL_ROOMS);
export const getAllRoomsSuccess = createAction(CONST.GET_ALL_ROOMS_SUCCESS);
export const getAllRoomsFail = createAction(CONST.GET_ALL_ROOMS_FAIL);

export const getAllServices = createAction(CONST.GET_ALL_SERVICES);
export const getAllServicesSuccess = createAction(CONST.GET_ALL_SERVICES_SUCCESS);
export const getAllServicesFail = createAction(CONST.GET_ALL_SERVICES_FAIL);

export const getBillById = createAction(CONST.GET_BILL_BY_ID);
export const getBillByIdSuccess = createAction(CONST.GET_BILL_BY_ID_SUCCESS);
export const getBillByIdFail = createAction(CONST.GET_BILL_BY_ID_FAIL);

export const roomChange = createAction(CONST.ROOM_CHANGE);
export const roomChangeSuccess = createAction(CONST.ROOM_CHANGE_SUCCESS);
export const roomChangeFail = createAction(CONST.ROOM_CHANGE_FAIL);

export const shiftChange = createAction(CONST.SHIFT_CHANGE);
export const shiftChangeSuccess = createAction(CONST.SHIFT_CHANGE_SUCCESS);
export const shiftChangeFail = createAction(CONST.SHIFT_CHANGE_FAIL);

export const createBill = createAction(CONST.CREATE_BILL);
export const createBillSuccess = createAction(CONST.CREATE_BILL_SUCCESS);
export const createBillFail = createAction(CONST.CREATE_BILL_FAIL);

export const payTheBill = createAction(CONST.PAY_THE_BILL);
export const payTheBillSuccess = createAction(CONST.PAY_THE_BILL_SUCCESS);
export const payTheBillFail = createAction(CONST.PAY_THE_BILL_FAIL);

export const addService = createAction(CONST.ADD_SERVICE);
export const addServiceSuccess = createAction(CONST.ADD_SERVICE_SUCCESS);
export const addServiceFail = createAction(CONST.ADD_SERVICE_FAIL);

export const delService = createAction(CONST.DEL_SERVICE);
export const delServiceSuccess = createAction(CONST.DEL_SERVICE_SUCCESS);
export const delServiceFail = createAction(CONST.DEL_SERVICE_FAIL);
/*---------------------------------------------------------------------*/

export const handleRoomDetail = createAction(CONST.HANDLE_ROOM_DETAIL);
export const handleInputClear = createAction(CONST.HANDLE_INPUT_CLEAR);
export const handleInputChange = createAction(CONST.HANDLE_INPUT_CHANGE);
export const handleSelectChange = createAction(CONST.HANDLE_SELECT_CHANGE);
export const handleValidate = createAction(CONST.HANDLE_VALIDATE);

export const handleRoomChange = createAction(CONST.HANDLE_ROOM_CHANGE);
export const handleAddService = createAction(CONST.HANDLE_ADD_SERVICE);
export const handleDelService = createAction(CONST.HANDLE_DEL_SERVICE);

export const handleClear = createAction(CONST.HANDLE_CLEAR);