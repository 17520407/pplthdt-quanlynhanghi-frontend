import React, { Component } from "react";
import { Col } from "reactstrap";
import moment from "moment";
import _ from "lodash";
import classnames from "classnames";

const checkStatusRoom = status => {
  if (status === "unused") {
    return <div className="badge badge-warning">Chưa sử dụng</div>;
  } else if (status === "used") {
    return <div className="badge badge-danger">Đang sử dụng</div>;
  } else {
    return <div className="badge badge-danger">Đang bảo trì</div>;
  }
};

class RoomItem extends Component {
  checkTypeRoom = type => {
    if (type === "cold") {
      return <span className="badge badge-purple">Máy lạnh</span>;
    } else {
      return <span className="badge badge-primary">Thường</span>;
    }
  };

  _handleAddCustomer = curRoom => {
    this.props.actions.handleInputClear("rentType");
    this.props.actions.toggleModal("customerModal");
    this.props.actions.handleRoomDetail({ data: curRoom });
  };

  _handleInfoCustomer = curRoom => {
    this.props.actions.handleRoomDetail({ data: curRoom, type: "edit" });
    this.props.actions.toggleModal("customerModal");
  };

  _handleRoomChange = curRoom => {
    if (!_.isEmpty(curRoom.customerIdentity)) {
      this.props.actions.handleRoomDetail({ data: curRoom });
      this.props.actions.handleRoomChange(curRoom);
      this.props.actions.handleInputClear("roomChange");
      this.props.actions.toggleModal("roomChangeModal");
    }
  };

  _handleGetBillById = curRoom => {
    this.props.actions.getBillById({
      billId: curRoom.billId,
      time: moment().format()
    });
    this.props.actions.toggleModal("feeModal");
  };

  render() {
    const { item } = this.props;
    return (
      <React.Fragment>
        <Col xl="3" md="6" sm="6">
          <div className="card-box room-box">
            <div className="dropdown pull-right">
              <a
                href="#"
                className="dropdown-toggle arrow-none card-drop"
                data-toggle="dropdown"
                aria-expanded="false"
              >
                <i className=" mdi mdi-arrow-down-drop-circle-outline" />
              </a>
              <div className="dropdown-menu dropdown-menu-right">
                <span
                  className={classnames("dropdown-item drop-down-tool", {
                    "cursor-not-allowed": _.isEmpty(item.customerIdentity)
                  })}
                  onClick={() => this._handleRoomChange(item)}
                >
                  <i className="mdi mdi-swap-horizontal text-primary" />
                  &nbsp; Đổi phòng
                </span>
              </div>
            </div>
            <h3 className="mt-0 text-bold">
              {_.isEmpty(item.name) ? item.roomName : item.name}
            </h3>
            <div className="status-bar">
              <div>Loại: {this.checkTypeRoom(item.type)}</div>
              {checkStatusRoom(item.status)}
            </div>

            {item.customerIdentity ? (
              <div>
                <ul className="">
                  <li>
                    <span>Họ tên:</span>
                    <span className="text-inverse text-bold">
                      {item.customerName}
                    </span>
                  </li>
                  <li>
                    <span>CMND:</span>
                    <span className="text-inverse">
                      {item.customerIdentity}
                    </span>
                  </li>
                  <li>
                    <span>Giờ vào:</span>
                    <strong className="text-success">
                      {moment(item.checkinDate).format("lll")}
                    </strong>
                  </li>
                  <li>
                    <span>Loại thuê:</span>
                    <strong className="text-purple">
                      {item.rentType === "perHour" ? "Theo giờ" : "Qua đêm"}
                    </strong>
                  </li>
                </ul>
                <div className="room-tool-container text-center">
                  <button
                    type="button"
                    className="btn btn-sm btn-primary btn-trans btn-rounded w-sm waves-effect waves-light"
                    // onClick={() => {
                    //   this.props.actions.handleRoomDetail({ data: item });
                    //   this.props.actions.toggleModal("serviceModal");
                    // }}
                    onClick={() => this._handleInfoCustomer(item)}
                  >
                    <i className="mdi mdi-food" />
                    &nbsp; Thông tin
                  </button>
                  <button
                    type="button"
                    className="btn btn-sm btn-success btn-rounded w-sm waves-effect waves-light"
                    onClick={() => this._handleGetBillById(item)}
                  >
                    <i className="mdi mdi-cash-usd" />
                    &nbsp; Thanh toán
                  </button>
                </div>
              </div>
            ) : (
              <div className="room-info-container">
                <h4 className="text-center">Phòng đang trống</h4>
                <div className="room-tool-container text-center">
                  <button
                    type="button"
                    className="btn btn-sm btn-primary btn-trans btn-rounded w-sm waves-effect waves-light"
                    onClick={() => this._handleAddCustomer(item)}
                    disabled={item.status === "fixed"}
                  >
                    <i className="mdi mdi-account-plus-outline"></i>&nbsp; Thêm
                    khách hàng
                  </button>
                </div>
              </div>
            )}
          </div>
        </Col>
      </React.Fragment>
    );
  }
}

export default RoomItem;
