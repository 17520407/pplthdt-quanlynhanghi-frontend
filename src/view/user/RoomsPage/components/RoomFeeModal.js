import React, { Component } from "react";
import { Modal, ModalHeader, ModalBody, ModalFooter, Table } from "reactstrap";
import moment from "moment";
import _ from "lodash";
import LoadingScreen from "../../../../modules/libs/LoadingScreen";

class RoomCustomerModal extends Component {
  _handlePayTheBill = () => {
    const { curRoom } = this.props;
    this.props.actions.payTheBill({
      billId: curRoom._id,
      checkoutDate: moment().format()
    });
  };

  render() {
    const {
      toggleFeeModal,
      curRoom,
      isLoadingBill,
      isProgressing
    } = this.props;
    return (
      <React.Fragment>
        <Modal
          isOpen={toggleFeeModal}
          toggle={() => this.props.actions.toggleModal("feeModal")}
        >
          <ModalHeader
            toggle={() => this.props.actions.toggleModal("feeModal")}
          >
            Thông tin phòng {curRoom.roomName}
          </ModalHeader>
          <ModalBody>
            {isLoadingBill ? (
              <LoadingScreen />
            ) : (
              <div className="modal-room-fee room-box">
                <h3 className="mt-0 text-bold">{curRoom.name}</h3>
                <ul className="">
                  <li>
                    <span>Khách hàng:</span>
                    <span className="text-inverse text-bold">
                      {curRoom.customerName}
                    </span>
                  </li>
                  <li>
                    <span>CMND:</span>
                    <span className="text-inverse">
                      {curRoom.customerIdentity}
                    </span>
                  </li>
                  <li>
                    <span>Giờ vào:</span>
                    <span className="text-success">
                      {moment(curRoom.checkinDate).format("lll")}
                    </span>
                  </li>
                  <li>
                    <span>Giờ ra:</span>
                    <span className="text-danger">
                      {moment().format("lll")}
                    </span>
                  </li>
                  <li>
                    <span>Loại thuê:</span>
                    {curRoom.rentType === "perHour" ? (
                      <span className="text-purple">
                        Theo giờ ({curRoom.pricePerHour} VNĐ)
                      </span>
                    ) : (
                      <span className="text-primary">
                        Qua đêm ({curRoom.pricePer12Hour} VNĐ)
                      </span>
                    )}
                  </li>
                  <li className="service-detail">
                    <Table responsive>
                      <thead>
                        <tr>
                          <th>Tên dịch vụ</th>
                          <th>Giá tiền</th>
                          <th>Số lượng</th>
                          <th>Tổng tiền</th>
                        </tr>
                      </thead>
                      <tbody>
                        {_.isEmpty(curRoom.services) ? (
                          <tr>
                            <td>Không có dịch vụ</td>
                          </tr>
                        ) : (
                          _.get(curRoom, "services", []).map((item, index) => {
                            return (
                              <tr key={index}>
                                <td>{item.serviceName}</td>
                                <td>{item.quantity}</td>
                                <td>{item.price} VNĐ</td>
                                <td>
                                  {Number.parseInt(item.price) *
                                    Number.parseInt(item.quantity)}{" "}
                                  VNĐ
                                </td>
                              </tr>
                            );
                          })
                        )}
                      </tbody>
                    </Table>
                  </li>
                </ul>
                <div className="d-flex jc-space-between">
                  <span className="txt-bold">Tổng tiền:</span>
                  <span className="txt-bold text-success fs-24">{curRoom.totalPrice} VNĐ</span>
                </div>
              </div>
            )}
          </ModalBody>
          <ModalFooter>
            <button
              type="button"
              className="btn btn-success btn-rounded w-md waves-effect waves-light"
              onClick={this._handlePayTheBill}
              disabled={isProgressing}
            >
              {isProgressing ? (
                <div>
                  <div className="spinner-border spinner-custom text-custom">
                    <span className="sr-only">Loading...</span>
                  </div>
                  &nbsp; Đang xử lí...
                </div>
              ) : (
                "Thanh toán"
              )}
            </button>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}

export default RoomCustomerModal;
