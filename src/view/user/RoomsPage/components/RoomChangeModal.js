import Select from "react-select";
import React, { Component } from "react";
import {
  Modal,
  ModalBody,
  ModalFooter,
  FormGroup,
  FormFeedback,
  Input
} from "reactstrap";
import _ from "lodash";

class RoomChangeModal extends Component {
  _handleValidate = () => {
    const { roomSelected } = this.props;
    if (_.isEmpty(roomSelected)) {
      this.props.actions.handleValidate({ checkType: "roomChange" });
      return false;
    } else {
      return true;
    }
  };

  componentDidUpdate(prevState) {
    const { isChangedRoom } = this.props;
    if (prevState.isChangedRoom !== isChangedRoom && isChangedRoom) {
      this.props.actions.getAllRooms();
      this.props.actions.handleInputClear("roomChange");
    }
  }

  _handleChangeRoom = () => {
    const { roomSelected, curRoom } = this.props;
    if (this._handleValidate()) {
      this.props.actions.roomChange({
        billId: curRoom.billId,
        roomId: roomSelected.value
      });
    }
  };

  _handleToggle = () => {
    this.props.actions.toggleModal("roomChangeModal");
    this.props.actions.handleInputClear("roomChange");
  };

  render() {
    const {
      toggleRoomChangeModal,
      roomSelected,
      isProgressing,
      roomOptions,
      isErrorRoomChange,
      curRoom
    } = this.props;
    return (
      <React.Fragment>
        <Modal isOpen={toggleRoomChangeModal} toggle={this._handleToggle}>
          <ModalBody>
            <FormGroup className="text-center">
              <h3>{curRoom.name}</h3>
              <i className="mdi mdi-swap-vertical fs-50 text-primary" />
            </FormGroup>
            <FormGroup>
              <label>Phòng trống hiện tại</label>
              <Select
                className="z-Index"
                options={roomOptions}
                onChange={option =>
                  this.props.actions.handleSelectChange({
                    option,
                    selectType: "roomChange"
                  })
                }
                value={roomSelected}
                isDisabled={isProgressing}
              />
              <Input type="hidden" invalid={isErrorRoomChange} />
              <FormFeedback>Bạn chưa chọn phòng để chuyển !</FormFeedback>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <button
              type="button"
              className="btn btn-success btn-rounded w-md waves-effect waves-light"
              onClick={this._handleChangeRoom}
              disabled={isProgressing}
            >
              {isProgressing ? (
                <div>
                  <div className="spinner-border spinner-custom text-custom">
                    <span className="sr-only">Loading...</span>
                  </div>
                  &nbsp; Đang xử lí...
                </div>
              ) : (
                <span>
                  <i className="mdi mdi-arrow-collapse-down" />
                  &nbsp;Xác nhận
                </span>
              )}
            </button>
            <button
              type="button"
              className="btn btn-secondary btn-rounded w-md waves-effect waves-light"
              onClick={this._handleToggle}
              disabled={isProgressing}
            >
              Đóng
            </button>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}

export default RoomChangeModal;
