import React, { Component } from "react";
import Select from "react-select";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  FormFeedback,
  Row,
  Col,
  Input
} from "reactstrap";
import _ from "lodash";

class RoomServiceModal extends Component {
  _handleValidate = () => {
    const { quantity, serviceName } = this.props.data;
    const { isErrorServiceName, isErrorServiceQuantity } = this.props;
    if (_.isEmpty(quantity) && _.isEmpty(serviceName)) {
      this.props.actions.handleValidate({ checkType: "service" });
      return false;
    } else {
      if (_.isEmpty(quantity)) {
        this.props.actions.handleValidate({
          checkType: "service",
          name: "quantity"
        });
        return false;
      } else if (_.isEmpty(serviceName)) {
        this.props.actions.handleValidate({
          checkType: "service",
          name: "serviceName"
        });
        return false;
      } else {
        if (isErrorServiceName || isErrorServiceQuantity) {
          return false;
        } else {
          return true;
        }
      }
    }
  };
  _handleAddService = () => {
    if (this._handleValidate()) {
      this.props.actions.handleAddService();
    }
  };

  render() {
    const {
      toggleServiceModal,
      serviceOptions,
      serviceSelected,
      isErrorServiceName,
      isErrorServiceQuantity,
      services,
      curRoom
    } = this.props;
    const { quantity, totalPrice } = this.props.data;
    return (
      <React.Fragment>
        <Modal
          isOpen={toggleServiceModal}
          // toggle={() => this.props.actions.toggleModal("serviceModal")}
          size="lg"
        >
          <ModalHeader
            toggle={() => this.props.actions.toggleModal("serviceModal")}
          >
            Thông tin dịch vụ
          </ModalHeader>
          <ModalBody>
            {_.isEmpty(serviceOptions) ? (
              <FormGroup>
                <p className="text-center">Danh dịch vụ hiện tại đang trống</p>
              </FormGroup>
            ) : (
              <FormGroup>
                <Row>
                  <Col lg="4" md="6" sm="6">
                    <FormGroup>
                      <label>Tên dịch vụ</label>
                      <Select
                        className="z-Index"
                        options={serviceOptions}
                        onChange={option =>
                          this.props.actions.handleSelectChange({
                            option,
                            selectType: "serviceName"
                          })
                        }
                        value={serviceSelected}
                      />
                      <Input type="hidden" invalid={isErrorServiceName} />
                      <FormFeedback>Vui lòng chọn dịch vụ</FormFeedback>
                    </FormGroup>
                  </Col>
                  <Col lg="4" md="6" sm="6">
                    <FormGroup>
                      <label>Số lượng hiện có trong kho</label>
                      <Input
                        disabled={true}
                        value={
                          _.isEmpty(serviceSelected)
                            ? ""
                            : serviceSelected.quantity
                        }
                      />
                    </FormGroup>
                  </Col>
                  <Col lg="4" md="12">
                    <FormGroup>
                      <label>Giá tiền mỗi sản phẩm</label>
                      <Input
                        disabled={true}
                        value={
                          _.isEmpty(serviceSelected)
                            ? ""
                            : serviceSelected.price
                        }
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <Row>
                  <Col lg="6">
                    <FormGroup>
                      <label>Số lượng</label>
                      <Input
                        placeholder="Số lượng"
                        type="text"
                        name="quantity"
                        onChange={e => this.props.actions.handleInputChange(e)}
                        value={quantity}
                        invalid={isErrorServiceQuantity}
                      />
                      <FormFeedback>
                        Số lượng phải là số và không lớn hơn số lượng hàng đang
                        có !
                      </FormFeedback>
                    </FormGroup>
                  </Col>
                  <Col lg="6">
                    <FormGroup>
                      <label>Tổng tiền</label>
                      <Input
                        type="text"
                        onChange={e => this.props.actions.handleInputChange(e)}
                        value={totalPrice}
                        disabled={true}
                      />
                    </FormGroup>
                  </Col>
                </Row>
              </FormGroup>
            )}
          </ModalBody>
          <ModalFooter>
            {!_.isEmpty(serviceOptions) && (
              <button
                type="button"
                className="btn btn-primary btn-rounded w-md waves-effect waves-light"
                onClick={this._handleAddService}
              >
                Thêm
              </button>
            )}
            <button
              type="button"
              className="btn btn-secondary btn-rounded w-md waves-effect waves-light"
              onClick={() => this.props.actions.toggleModal("serviceModal")}
            >
              Đóng
            </button>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}

export default RoomServiceModal;
