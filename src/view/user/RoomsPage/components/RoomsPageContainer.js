import React, { Component } from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { name } from "../reducers";
import * as action from "../actions";
import RoomsPage from "./RoomsPage";
class RoomsPageContainer extends Component {
  UNSAFE_componentWillMount() {
    this.props.actions.handleClear();
  }

  componentWillMount() {
    this.props.actions.getAllRooms();
    this.props.actions.getAllServices();
  }
  render() {
    return (
      <React.Fragment>
        <RoomsPage {...this.props} />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...state[name]
  };
}
function mapDispatchToProps(dispatch) {
  const actions = {
    ...action
  };
  return { actions: bindActionCreators(actions, dispatch) };
}
export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(RoomsPageContainer)
);
