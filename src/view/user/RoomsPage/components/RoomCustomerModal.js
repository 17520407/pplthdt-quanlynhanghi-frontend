import React, { Component } from "react";
import Select from "react-select";
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  FormGroup,
  FormFeedback,
  Input,
  Table
} from "reactstrap";
import _ from "lodash";
import moment from "moment";

class RoomCustomerModal extends Component {
  _handleValidate = () => {
    const { customerName, customerIdentity, rentType } = this.props.data;
    const {
      isErrorCustomerName,
      isErrorCustomerIdentity,
      isErrorCustomerRentType
    } = this.props;
    if (
      _.isEmpty(customerName) &&
      _.isEmpty(customerIdentity) &&
      _.isEmpty(rentType)
    ) {
      this.props.actions.handleValidate({ checkType: "bill" });
      return false;
    } else {
      if (_.isEmpty(customerName)) {
        this.props.actions.handleValidate({
          name: "customerName",
          checkType: "bill"
        });
        return false;
      } else if (_.isEmpty(customerIdentity)) {
        this.props.actions.handleValidate({
          name: "customerIdentity",
          checkType: "bill"
        });
        return false;
      } else if (_.isEmpty(rentType)) {
        this.props.actions.handleValidate({
          name: "rentType",
          checkType: "bill"
        });
        return false;
      } else {
        if (
          isErrorCustomerName ||
          isErrorCustomerIdentity ||
          isErrorCustomerRentType
        ) {
          return false;
        } else {
          return true;
        }
      }
    }
  };

  _handleAddCustomer = () => {
    const { curRoom, isUpdate, services } = this.props;
    const { customerName, customerIdentity, rentType } = this.props.data;

    if (isUpdate) {
      this.props.actions.addService({
        billId: curRoom.billId,
        services
      });
    } else {
      if (this._handleValidate()) {
        let curDate = moment().format();
        this.props.actions.createBill({
          customerName,
          customerIdentity,
          rentType,
          roomId: curRoom._id,
          checkinDate: curDate
        });
      }
    }
  };

  componentDidUpdate(prevState) {
    const { isAddCustomer, services, curRoom } = this.props;
    if (prevState.isAddCustomer !== isAddCustomer && isAddCustomer) {
      this.props.actions.addService({ billId: curRoom._id, services });
      //this.props.actions.handleInputClear("service");
    }
  }

  _handleClose = () => {
    this.props.actions.getAllServices();
    this.props.actions.toggleModal("customerModal");
    this.props.actions.handleInputClear("service");
  };

  _handleDelService = item => {
    const { isUpdate, curRoom } = this.props;
    if (isUpdate) {
      this.props.actions.delService({
        billId: curRoom.billId,
        serviceName: item.serviceName
      });
      this.props.actions.handleDelService(item);
    } else {
      this.props.actions.handleDelService(item);
    }
  };

  render() {
    const {
      toggleCustomerModal,
      isErrorCustomerName,
      isErrorCustomerIdentity,
      isErrorCustomerRentType,
      rentTypeSelected,
      isProgressing,
      rentType,
      services,
      isUpdate
    } = this.props;

    const { customerName, customerIdentity } = this.props.data;
    return (
      <React.Fragment>
        <Modal isOpen={toggleCustomerModal}>
          <ModalHeader toggle={this._handleClose}>Thông tin phòng</ModalHeader>
          <ModalBody>
            <FormGroup>
              <label>Họ tên khách hàng</label>
              <Input
                type="text"
                name="customerName"
                value={customerName}
                onChange={e => this.props.actions.handleInputChange(e)}
                invalid={isErrorCustomerName}
                maxLength="100"
                disabled={isProgressing || isUpdate}
              />
              <FormFeedback>Bạn chưa nhập tên khách hàng !</FormFeedback>
            </FormGroup>
            <FormGroup>
              <label>Chứng minh nhân nhân</label>
              <Input
                type="text"
                name="customerIdentity"
                value={customerIdentity}
                onChange={e => this.props.actions.handleInputChange(e)}
                invalid={isErrorCustomerIdentity}
                maxLength="50"
                disabled={isProgressing || isUpdate}
              />
              <FormFeedback>
                Bạn chưa nhập chứng minh nhân dân khách hàng !
              </FormFeedback>
            </FormGroup>
            <FormGroup>
              <label>Loại thuê</label>
              <Select
                className="z-Index"
                options={rentType}
                onChange={option =>
                  this.props.actions.handleSelectChange({
                    option,
                    selectType: "rentType"
                  })
                }
                value={rentTypeSelected}
                isDisabled={isProgressing || isUpdate}
              />
              <Input type="hidden" invalid={isErrorCustomerRentType} />
              <FormFeedback>Bạn chưa chọn loại thuê phòng !</FormFeedback>
            </FormGroup>
            <FormGroup>
              <label>Dịch vụ</label>
              &nbsp;
              <div className="text-center">
                <Table responsive>
                  <thead>
                    <tr className="text-center">
                      <th>Tên dịch vụ</th>
                      <th>Giá tiền</th>
                      <th>Số lượng</th>
                      <th>Tổng tiền</th>
                      <th>#</th>
                    </tr>
                  </thead>
                  <tbody>
                    {_.isEmpty(services) ? (
                      <tr>
                        <td className="text-center">Chưa có dịch vụ.</td>
                      </tr>
                    ) : (
                      services.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td className="text-center">{item.serviceName}</td>
                            <td className="text-center">{item.price}</td>
                            <td className="text-center">{item.quantity}</td>
                            <td className="text-center">{item.totalPrice}</td>

                            <td className="text-center">
                              <span
                                className="cursor-pointer"
                                onClick={() => this._handleDelService(item)}
                              >
                                <i className="mdi mdi-trash-can-outline text-danger" />
                              </span>
                            </td>
                          </tr>
                        );
                      })
                    )}
                  </tbody>
                </Table>
                <button
                  className="btn btn-icon btn-trans waves-effect waves-light btn-primary m-b-5"
                  onClick={() => this.props.actions.toggleModal("serviceModal")}
                  disabled={isProgressing}
                >
                  <i className="mdi mdi-cart-plus" />
                </button>
              </div>
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <button
              type="button"
              className="btn btn-primary btn-rounded w-md waves-effect waves-light"
              onClick={this._handleAddCustomer}
              disabled={isProgressing}
            >
              {isUpdate ? "Cập nhật" : "Thêm"}
            </button>
            {/* &nbsp;
            <button
              type="button"
              className="btn btn-secondary btn-rounded w-md waves-effect waves-light"
              disabled={isProgressing}
            >
              Huỷ
            </button> */}
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}

export default RoomCustomerModal;
