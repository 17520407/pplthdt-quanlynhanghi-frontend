import axios from "axios";
import _ from "lodash";
import { history } from "../stores";
import config from "../config";
import { save, clearAll, get } from "services/localStoredService";
import { NotificationManager } from "react-notifications";

export const refresh = (requestData, refreshToken) => {
  let authAdmin = get("isAdmin");
  let urlAPI;
  if (authAdmin) {
    urlAPI = `${config.apiBaseURL}/admin/refresh`;
  } else {
    urlAPI = `${config.apiBaseURL}/users/refresh`;
  }

  return axios({
    method: "GET",
    url: urlAPI,
    headers: {
      authorization: refreshToken,
      "Content-Type": "application/json"
    }
  })
    .then(async res => {
      const { accessToken } = res.data.message;
      if (!_.isEmpty(accessToken)) {
        save("accessToken", accessToken);
        const { endpoint, method, data, config } = requestData;
        return request(endpoint, method, data, config, accessToken);
      } else {
        history.push("/login");
        window.location.reload();
        clearAll();
      }
    })
    .catch(err => {
      history.push("/login");
      window.location.reload();
      // NotificationManager.error("Lỗi", err.stack, 3000);
      clearAll();
      return err;
    });
};

export const handerError = async (error, requestData) => {
  return new Promise(async (resolve, reject) => {
    const status = _.get(error, "response.status");
    const refreshToken = get("refreshToken");
    if (status >= 400 && status < 500) {
      if (status === 403) {
        return resolve(_.get(error, "response", ""));
      } else if (status === 404) {
        NotificationManager.error("Không tìm thấy", "Opps.. !", 2000);
        // history.push("/404");
        // window.location.reload();
        return error;
      } else {
        if (status === 401) {
          if (!_.isEmpty(refreshToken)) {
            return resolve(await refresh(requestData, refreshToken));
          } else {
            history.push("/login");
            window.location.reload();
            clearAll();
            return;
          }
        } else {
          return resolve(_.get(error, "response", ""));
        }
      }
    } else {
      history.push("/login");
      window.location.reload();
      return resolve(_.get(error, "response", ""));
    }
  });
};

export const CONTENT_TYPE = {
  form: "application/x-www-form-urlencoded",
  json: "application/json"
};

export const request = (
  endpoint,
  method,
  data,
  config = false,
  accessToken = null
) => {
  return new Promise((resolve, reject) => {
    const getHeaders = (contentType = CONTENT_TYPE.json) => {
      const token = accessToken ? accessToken : get("accessToken");
      const header = {
        authorization: token,
        "Content-Type": contentType
      };
      return header;
    };

    const headers = !config ? getHeaders() : getHeaders(CONTENT_TYPE.form);
    const options = {
      method,
      url: endpoint,
      headers,
      data: method !== "GET" ? data : null,
      params: method === "GET" ? data : null
    };
    return axios(options)
      .then(res => {
        return resolve(res);
      })
      .catch(async error => {
        try {
          return resolve(
            await handerError(error, { endpoint, method, data, config })
          );
        } catch (err) {
          return reject(error);
        }
      });
  });
};
