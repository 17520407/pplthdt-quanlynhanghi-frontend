import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { get } from "../services/localStoredService";

const authAdmin = get("isAdmin");

class Main extends Component {
  render() {
    if (authAdmin) {
      return <Redirect to="/admin" />;
    } else {
      return <Redirect to="/user" />;
    }
  }
}
export default Main;
