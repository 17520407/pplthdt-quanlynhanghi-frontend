import React from "react";
import {  Route, Switch } from "react-router-dom";
import ServicesPage from "../../../view/admin/ServicesPage/components/ServicesPageContainer";

const EmployeeController = ({ match }) => (
    <Switch>
      <Route path={match.url} component={ServicesPage} />
    </Switch>
);
export default EmployeeController;
