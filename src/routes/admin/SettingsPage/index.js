import React from "react";
import { Route, Switch } from "react-router-dom";
import SettingsPage from "../../../view/admin/SettingsPage/components/SettingsPageContainer";

const EmployeeController = ({ match }) => (
    <Switch>
      <Route path={match.url} component={SettingsPage} />
    </Switch>
);
export default EmployeeController;
