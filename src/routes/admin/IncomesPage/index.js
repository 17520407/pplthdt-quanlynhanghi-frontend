import React from "react";
import { Route, Switch } from "react-router-dom";
import IncomesPage from "../../../view/admin/IncomesPage/components/IncomesPageContainer";

const IncomeController = ({ match }) => (
  <Switch>
    <Route path={match.url} component={IncomesPage} />
  </Switch>
);
export default IncomeController;
