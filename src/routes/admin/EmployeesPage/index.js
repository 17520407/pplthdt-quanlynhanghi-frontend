import React from "react";
import {  Route, Switch } from "react-router-dom";
import EmployeesPage from "../../../view/admin/EmployeesPage/components/EmployeesPageContainer";

const EmployeeController = ({ match }) => (
    <Switch>
      <Route path={match.url} component={EmployeesPage} />
    </Switch>
);
export default EmployeeController;
