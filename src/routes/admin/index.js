import React, { Component } from "react";
import { Route, withRouter, Switch, Redirect } from "react-router-dom";
import WrappedMenuContainer from "../../modules/WrappedMenu/components/WrappedMenuContainer";
import Employees from "./EmployeesPage";
import Rooms from "./RoomsPage";
import Services from "./ServicesPage";
import Settings from "./SettingsPage";
import Incomes from "./IncomesPage";
import Reports from "./ReportsPage";

class App extends Component {
  render() {
    const { match } = this.props;

    return (
      <WrappedMenuContainer>
        <Switch>
          <Redirect exact from={`${match.url}`} to={`${match.url}/employees`} />
          <Route path={`${match.url}/employees`} component={Employees} />
          <Route path={`${match.url}/rooms`} component={Rooms} />
          <Route path={`${match.url}/incomes`} component={Incomes} />
          <Route path={`${match.url}/services`} component={Services} />
          <Route path={`${match.url}/reports`} component={Reports} />
          <Route path={`${match.url}/settings`} component={Settings} />
        </Switch>
      </WrappedMenuContainer>
    );
  }
}

export default withRouter(App);
