import React from "react";
import { Route, Switch } from "react-router-dom";
import ProfilePage from "../../../view/user/ProfilePage/components/ProfilePageContainer";

const ProfileController = ({ match }) => (
    <Switch>
      <Route path={match.url} component={ProfilePage} />
    </Switch>
);
export default ProfileController;
