import React from "react";
import { Route, Switch } from "react-router-dom";
import RoomsPage from "../../../view/user/RoomsPage/components/RoomsPageContainer";

const RoomsController = ({ match }) => (
  <Switch>
    <Route path={match.url} component={RoomsPage} />
  </Switch>
);
export default RoomsController;
