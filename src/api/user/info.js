import config from "../../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/users/info`;

export function getUserInfo(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "GET");
}

export function updateUserInfo(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "PUT", data);
}
