import config from "../../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/users/bills`;

export function getBillById(data) {
  const endpoint = `${baseEndpoint}/${data.billId}`;
  return request(endpoint, "POST", { time: data.time });
}

export function createBill(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "POST", data);
}

export function payTheBill(data) {
  const endpoint = `${baseEndpoint}/paid/${data.billId}`;
  return request(endpoint, "POST", data);
}

export function addService(data) {
  const endpoint = `${baseEndpoint}/services/${data.billId}`;
  return request(endpoint, "PUT", data);
}

export function delService(data) {
  const endpoint = `${baseEndpoint}/services/${data.billId}`;
  return request(endpoint, "DELETE", data);
}

export function roomChange(data) {
  const endpoint = `${baseEndpoint}/rooms/${data.billId}`;
  return request(endpoint, "PUT", data);
}
