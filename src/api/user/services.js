import config from "../../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/users/services`;

export function getAllServices(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "GET");
}


