import config from "../../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/admin/services`;

export function getAllServices(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "GET");
}

export function addNewService(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "POST", data);
}

export function updateService(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "PUT", data);
}

export function delService(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "DELETE");
}
