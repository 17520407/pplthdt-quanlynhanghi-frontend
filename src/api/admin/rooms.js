import config from "../../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/admin/rooms`;

export function getAllRooms(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "GET");
}

export function addNewRoom(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "POST", data);
}

export function updateRoom(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "PUT", data);
}

export function delRoom(data) {
  const endpoint = `${baseEndpoint}/${data._id}`;
  return request(endpoint, "DELETE");
}
