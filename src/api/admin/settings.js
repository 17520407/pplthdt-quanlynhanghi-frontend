import config from "../../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/admin/params`;

export function getSettings(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "GET");
}

export function updateSettings(data) {
  const endpoint = `${baseEndpoint}`;
  return request(endpoint, "PUT",data);
}