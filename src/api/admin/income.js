import config from "../../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/admin`;

export function getIncomes(data) {
  const endpoint = `${baseEndpoint}/incomes`;
  return request(endpoint, "POST", data);
}

export function moneyDraw(data) {
  const endpoint = `${baseEndpoint}/incomes/takeMoney`;
  return request(endpoint, "POST", data);
}
