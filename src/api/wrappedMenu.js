import config from "../config";
import axios from "axios";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/auth`;

export function login(data) {
  const endpoint = `${baseEndpoint}/login`;
  return request(endpoint, "POST", data);
}

export function logout(data) {
  const endpoint = `${baseEndpoint}/logout`;
  return axios.post(endpoint, {});
}
