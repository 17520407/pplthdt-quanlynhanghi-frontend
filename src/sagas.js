/**
 * @file init sagas
 */

import { all } from "redux-saga/effects";

// Place for sagas' app
import { sagas as WrappedMenu } from "modules/WrappedMenu";
import { sagas as LoginPage } from "modules/LoginPage";
/*---------------------------------------------------------------*/
import { sagas as AdminReportPage } from "view/admin/ReportsPage";
import { sagas as AdminRoomsPage } from "view/admin/RoomsPage";
import { sagas as AdminEmployeesPage } from "view/admin/EmployeesPage";
import { sagas as AdminIncomesPage } from "view/admin/IncomesPage";
import { sagas as AdminServicesPage } from "view/admin/ServicesPage";
import { sagas as AdminSettingsPage } from "view/admin/SettingsPage";
/*---------------------------------------------------------------*/
import { sagas as UserProfilePage } from "view/user/ProfilePage";
import { sagas as UserRoomsPage } from "view/user/RoomsPage";

/*----Saga Root List-----------------*/
let sagasList = [
  WrappedMenu(),
  LoginPage(),
  AdminReportPage(),
  AdminRoomsPage(),
  AdminEmployeesPage(),
  AdminSettingsPage(),
  AdminServicesPage(),
  AdminIncomesPage(),
  UserRoomsPage(),
  UserProfilePage()
];

export default function* rootSaga(getState) {
  yield all(sagasList);
}
