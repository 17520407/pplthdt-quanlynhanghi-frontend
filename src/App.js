import React, { Component } from "react";
import {
  Switch,
  Route,
  // Redirect,
  BrowserRouter as Router
} from "react-router-dom";
import { NotificationContainer } from "react-notifications";
import "react-notifications/lib/notifications.css";
import LoginPage from "modules/LoginPage/components/LoginPageContainer";
import Page404 from "modules/common/Page404/index.js";
import main from "./routes";
import admin from "./routes/admin";
import user from "./routes/user";

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <NotificationContainer />

        <Router>
          <Switch>
            <Route path="/login" exact component={LoginPage} />
            <Route path="/404" exact component={Page404} />
            <Route path="/admin" component={admin} />
            <Route path="/user" component={user} />
            <Route path="/" exact component={main} />
            {/* <Route
            path="*"
            render={props => {
              if (
                ![
                  "/login",
                  "/",
                  "/rooms",
                  "/report",
                  "/admin/employees",
                  "/services",
                  "/incomes",
                  "/settings",
                  "/admin",
                  "/user"
                ].includes(props.location.pathname)
              ) {
                return <Redirect to="/404" from="*" />;
              }
            }} */}
            />
          </Switch>
        </Router>
      </React.Fragment>
    );
  }
}

export default App;
