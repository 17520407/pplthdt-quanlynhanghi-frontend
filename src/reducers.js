/**
 * @file reducers
 */

import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

// Place for reducers' app
import WrappedMenu, { name as nameOfWrappedMenu } from "modules/WrappedMenu";
import LoginPage, { name as nameOfLoginPage } from "modules/LoginPage";

/*---------------------------------------------------------------*/
import AdminReportsPage, {
  name as nameOfAdminReportsPage
} from "view/admin/ReportsPage";

import AdminRoomsPage, {
  name as nameOfAdminRoomsPage
} from "view/admin/RoomsPage";
import AdminEmployeesPage, {
  name as nameOfAdminEmployeesPage
} from "view/admin/EmployeesPage";
import AdminServicesPage, {
  name as nameOfAdminServicesPage
} from "view/admin/ServicesPage";
import AdminIncomesPage, {
  name as nameOfAdminIncomesPage
} from "view/admin/IncomesPage";
import AdminSettingsPage, {
  name as nameOfAdminSettingsPage
} from "view/admin/SettingsPage";
/*---------------------------------------------------------------*/
import UserRoomsPage, {
  name as nameOfUserRoomsPage
} from "view/user/RoomsPage";
import UserProfilePage, {
  name as nameOfUserProfilePage
} from "view/user/ProfilePage";

/*-----------Combine Reducers --------------*/
let reducers = {
  [nameOfWrappedMenu]: WrappedMenu,
  [nameOfLoginPage]: LoginPage,
  [nameOfAdminReportsPage]: AdminReportsPage,
  [nameOfAdminRoomsPage]: AdminRoomsPage,
  [nameOfAdminEmployeesPage]: AdminEmployeesPage,
  [nameOfAdminServicesPage]: AdminServicesPage,
  [nameOfAdminSettingsPage]: AdminSettingsPage,
  [nameOfAdminIncomesPage]: AdminIncomesPage,
  [nameOfUserRoomsPage]: UserRoomsPage,
  [nameOfUserProfilePage]: UserProfilePage
};

export default history =>
  combineReducers({
    ...reducers,
    router: connectRouter(history)
  });
