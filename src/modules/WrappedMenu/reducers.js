/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "WrappedMenu";

const initialState = freeze({
  token: ""
});

export default handleActions(
  {
    [actions.saveToken]: (action, state) => {
      return freeze({
        ...state,
        token: action.payload
      });
    }
  },
  initialState
);
