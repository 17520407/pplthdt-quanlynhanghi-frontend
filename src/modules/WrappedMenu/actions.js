/**
 * @file All actions will be listed here
 */

import { createAction } from 'redux-actions';
import * as CONST from './constants';

export const saveToken = createAction(CONST.SAVE_TOKEN);
export const saveRole = createAction(CONST.SAVE_ROLE);

export const checkLogin = createAction(CONST.CHECK_LOGIN);
export const checkLoginSuccess = createAction(CONST.CHECK_LOGIN_SUCCESS);
export const checkLoginFail = createAction(CONST.CHECK_LOGIN_FAIL);

export const logOut = createAction(CONST.LOG_OUT);

