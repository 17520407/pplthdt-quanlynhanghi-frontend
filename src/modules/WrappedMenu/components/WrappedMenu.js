import React, { Component } from "react";

import UserSideBar from "./UserSidebar";
import AdminSideBar from "./AdminSidebar";
import { get } from "../../../services/localStoredService";

let authAdmin;
class WrappedMenu extends Component {
  UNSAFE_componentWillMount() {
    authAdmin = get("isAdmin");
  }

  componentDidMount() {
    authAdmin = get("isAdmin");
  }

  render() {
    return (
      <React.Fragment>
        <div id="wrapper">
          <div className="topbar">
            <div className="topbar-left">
              <span className="logo">
                <span>
                  Quản lý <span> nhà nghỉ</span>
                </span>
                <i className="mdi mdi-layers" />
              </span>
            </div>

            <div className="navbar navbar-default" role="navigation">
              <div className="container-fluid">
                <ul className="nav navbar-nav list-inline navbar-left">
                  <li className="list-inline-item">
                    <button className="button-menu-mobile open-left">
                      <i className="mdi mdi-menu" />
                    </button>
                  </li>
                  <li className="list-inline-item">
                    <h4 className="page-title">Trang quản trị</h4>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div className="left side-menu">
            <div className="sidebar-inner slimscrollleft">
              <div className="user-box">
                <h5>
                  {authAdmin ? (
                    <span style={{ color: "#fff" }}>Quản trị viên</span>
                  ) : (
                    <span style={{ color: "#fff" }}>Nhân viên</span>
                  )}
                </h5>
                <ul className="list-inline">
                  {/* <li className="list-inline-item">
                    <a href="#">
                      <i className="mdi mdi-settings" />
                    </a>
                  </li> */}

                  <li className="list-inline-item">
                    <span
                      className="text-custom"
                      style={{ cursor: "pointer" }}
                      onClick={() =>
                        this.props.actions.logOut({
                          history: this.props.history
                        })
                      }
                    >
                      <i className="mdi mdi-power" />
                    </span>
                  </li>
                </ul>
              </div>

              <div id="sidebar-menu">
                {authAdmin ? (
                  <AdminSideBar {...this.props} />
                ) : (
                  <UserSideBar {...this.props} />
                )}
              </div>
              <div className="clearfix" />
            </div>
          </div>

          <div className="content-page">
            <div className="content">
              <div className="container-fluid">{this.props.children}</div>
            </div>

            <footer className="footer text-right">
              Website created by D2P
            </footer>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default WrappedMenu;
