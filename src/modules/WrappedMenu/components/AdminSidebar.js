import React from "react";
import { Link } from "react-router-dom";

const AdminSidebar = props => {
  return (
    <React.Fragment>
      <ul>
        <li className="text-muted menu-title">Thanh điều hướng</li>

        <li>
          <Link
            to="/admin/employees"
            className={
              props.location.pathname === "/admin/employees"
                ? "waves-effect active"
                : "waves-effect"
            }
          >
            <i className="mdi mdi-account-multiple" /> <span> Nhân viên </span>{" "}
          </Link>
        </li>
        <li>
          <Link
            to="/admin/rooms"
            className={
              props.location.pathname === "/admin/rooms"
                ? "waves-effect active"
                : "waves-effect"
            }
          >
            <i className="mdi mdi-bed-empty" /> <span> Phòng </span>{" "}
          </Link>
        </li>
        <li>
          <Link
            to="/admin/services"
            className={
              props.location.pathname === "/admin/services"
                ? "waves-effect active"
                : "waves-effect"
            }
          >
            <i className="mdi mdi-food" /> <span> Dịch vụ </span>{" "}
          </Link>
        </li>
        <li>
          <Link
            to="/admin/incomes"
            className={
              props.location.pathname === "/admin/incomes"
                ? "waves-effect active"
                : "waves-effect"
            }
          >
            <i className="mdi mdi-cash-refund" /> <span> Doanh thu </span>{" "}
          </Link>
        </li>
        <li>
          <Link
            to="/admin/reports"
            className={
              props.location.pathname === "/admin/reports"
                ? "waves-effect active"
                : "waves-effect"
            }
          >
            <i className="mdi mdi-chart-areaspline" /> <span> Báo cáo </span>{" "}
          </Link>
        </li>
        <li>
          <Link
            to="/admin/settings"
            className={
              props.location.pathname === "/admin/settings"
                ? "waves-effect active"
                : "waves-effect"
            }
          >
            <i className="mdi mdi-settings" /> <span> Cài đặt tham số </span>{" "}
          </Link>
        </li>
      </ul>
      <div className="clearfix" />
    </React.Fragment>
  );
};

export default AdminSidebar;
