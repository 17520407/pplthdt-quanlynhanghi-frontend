import React from "react";
import { Link } from "react-router-dom";

const UserSidebar = props => {
  return (
    <React.Fragment>
      <ul>
        <li className="text-muted menu-title">Thanh điều hướng</li>
        <li>
          <Link
            to="/user/info"
            className={
              props.location.pathname === "/user/info"
                ? "waves-effect active"
                : "waves-effect"
            }
          >
            <i className="mdi mdi-account" /> <span> Tài khoản </span>
          </Link>
        </li>
        <li>
          <Link
            to="/user/rooms"
            className={
              props.location.pathname === "/user/rooms"
                ? "waves-effect active"
                : "waves-effect"
            }
          >
            <i className="mdi mdi-bed-empty" /> <span> Phòng </span>
          </Link>
        </li>
      </ul>
      <div className="clearfix" />
    </React.Fragment>
  );
};

export default UserSidebar;
