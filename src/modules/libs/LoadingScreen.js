import React, { Component } from "react";
import { Col } from "reactstrap";

export default class LoadingScreen extends Component {
  render() {
    return (
      <Col
        lg="12"
        className="text-center"
        style={{
          margin: "20px 0",
          height: "50vh",
          display: "flex",
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <div className="spinner-border text-custom m-2" role="status">
          <span className="sr-only">Loading...</span>
        </div>
      </Col>
    );
  }
}
