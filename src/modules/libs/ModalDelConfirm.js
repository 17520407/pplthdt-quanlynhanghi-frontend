import React from "react";
import { Modal, ModalBody, ModalFooter, Button } from "reactstrap";

const ModalDelConfirm = ({ isToggle, title, handleToggle, handleDel }) => {
  console.log(isToggle);
  return (
    <Modal isOpen={isToggle} toggle={handleToggle} centered>
      <ModalBody>
        <div className="text-center">
          <i className="mdi mdi-alert-decagram fs-50 text-danger" />
          <p>{title}</p>
        </div>
      </ModalBody>
      <ModalFooter>
        <Button color="danger" onClick={handleDel}>
          Xoá
        </Button>
        <Button color="secondary" onClick={handleToggle}>
          Huỷ
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalDelConfirm;
