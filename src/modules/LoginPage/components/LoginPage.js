import React, { Component } from "react";
import { Input, Button } from "reactstrap";
import { Link, withRouter } from "react-router-dom";

class LoginPage extends Component {
  render() {
    const { username, password } = this.props.data;

    return (
      <React.Fragment>
        <div className="account-pages" />
        <div className="clearfix" />
        <div className="wrapper-page">
          <div className="text-center">
            <Link to="/login" className="logo">
              <span>
                Quản lí <span> nhà nghỉ</span>
              </span>
            </Link>
            <h5 className="text-muted m-t-0 font-600">Trang quản trị</h5>
          </div>
          <div className="m-t-40 card-box">
            <div className="text-center">
              <h4 className="text-uppercase font-bold m-b-0">Đăng nhập</h4>
            </div>
            <div className="p-20">
              <form className="form-horizontal m-t-20">
                <div className="form-group">
                  <div className="col-xs-12">
                    <Input
                      className="form-control"
                      type="text"
                      name="username"
                      required
                      value={username}
                      onChange={e => this.props.actions.handleInputChange(e)}
                      placeholder="Username"
                    />
                  </div>
                </div>

                <div className="form-group">
                  <div className="col-xs-12">
                    <Input
                      className="form-control"
                      type="password"
                      name="password"
                      required
                      value={password}
                      onChange={e => this.props.actions.handleInputChange(e)}
                      placeholder="Password"
                      onKeyPress={event => {
                        if (event.key === "Enter") {
                          this.props.actions.login({
                            data: this.props.data,
                            history: this.props.history
                          });
                        }
                      }}
                    />
                  </div>
                </div>
                <div className="form-group text-center m-t-30">
                  <div className="col-xs-12">
                    <Button
                      className="btn btn-custom btn-bordred btn-block waves-effect waves-light"
                      onClick={() =>
                        this.props.actions.login({
                          data: this.props.data,
                          history: this.props.history
                        })
                      }
                    >
                      Đăng Nhập
                    </Button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default withRouter(LoginPage);
