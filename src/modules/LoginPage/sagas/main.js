import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import * as actionsWrappedMenu from "modules/WrappedMenu/actions";
import * as wrappedMenuAPI from "api/wrappedMenu";
import { takeAction } from "services/forkActionSagas";
import { save, clearAll } from "services/localStoredService";
import { NotificationManager } from "react-notifications";
import _ from "lodash";

export function* handleLogin(action) {
  try {
    let res = yield call(wrappedMenuAPI.login, action.payload.data);
    console.log(res);
    if (res.data.success) {
      const { accessToken, refreshToken, isAdmin } = res.data.data;
      if (!_.isEmpty(refreshToken)) {
        save("accessToken", accessToken);
        save("isAdmin", isAdmin);
        save("refreshToken", refreshToken);
        yield put(actionsWrappedMenu.saveRole(isAdmin));
      } else {
        save("accessToken", accessToken);
      }
      yield put(actions.loginSuccess(res.data));
      yield put(actionsWrappedMenu.saveToken(accessToken));

      if (isAdmin) {
        action.payload.history.push("/admin");
      } else {
        action.payload.history.push("/user");
      }
    } else {
      NotificationManager.error(
        "Tên tài khoản hoặc mật khẩu không hợp lệ !",
        "Opps..",
        2000
      );
      yield put(actions.loginFail(res.data.message));
    }
  } catch (err) {
    yield put(actions.loginFail(err));
  }
}

export function* handleLogout(action) {
  try {
    clearAll();
    yield call(wrappedMenuAPI.logout);
    action.payload.history.push("/login");
    yield put(actions.logoutSuccess());
  } catch (err) {}
}

export function* login() {
  yield takeAction(actions.login, handleLogin);
}

export function* logout() {
  yield takeAction(actions.logout, handleLogout);
}

export default [login, logout];
